<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    protected $_data;
    private $_ES;
    private $HOST;
    public $REDIS_HOST;
    public $REDIS_PORT;
    public $APP_ID_NAME_PKG;
    public $APP_ID_NAME_MODEL;
    public $URL_ID_NAME;

    /**
     * Constructor for MY controller
     * This controller is main controller extended by almost all controllers in this application
     * 
     * @return void
     * @author ronaldlayanto
     */
    public function __construct() 
    {
        parent::__construct();
		$this->load->model('auth_model', 'auth_model');
        $this->load->model('menu_model', 'menu_model');
        $this->HOST = $this->config->item('ES_HOST');
        $this->REDIS_HOST = $this->config->item('REDIS_HOST');
        $this->REDIS_PORT = $this->config->item('REDIS_PORT');
        /*
         * construct elasticsearch
         */
        $this->_ES = Elasticsearch\ClientBuilder::create()
            ->setHosts($this->HOST)
            ->setRetries(0)
            ->build();
        /*
         * Construct Redis
         */

        $this->REDIS = new Predis\Client([
            'scheme' => 'tcp',
            'host'   => $this->REDIS_HOST,
            'port'   => $this->REDIS_PORT,
        ]);
        /*
         * get app id Name
         */
        $app_id_pkg = json_decode($this->REDIS->get("package_key"));
        $app_id_model = json_decode($this->REDIS->hget("APP_1","model"));
        $tag_data = json_decode($this->REDIS->get("finance_key"),true);
        $url_id_map = json_decode($this->REDIS->get("url_key"));

        foreach($app_id_pkg->data as $url_id){
            $this->APP_ID_NAME_PKG[$url_id->id] = $url_id->title;
        }
        foreach($app_id_model as $url_id){
            $this->APP_ID_NAME_MODEL[$url_id->id] = $url_id->model;
        }
        foreach($url_id_map->data as $url_id){
            $this->URL_ID_NAME[$url_id->id] = $url_id->up_name;
        }


        //$this->REDIS->close();

        
        if($this->auth_model->is_logged()) 
        {
            $session_data = $this->session->userdata('mc_admin_session');
            $this->data['admin_id'] = $session_data['admin_id'];
            $this->data['admin_uname'] = $session_data['admin_uname'];
            $this->data['admin_group_id'] = $session_data['admin_group_id'];
            $this->form_validation->set_error_delimiters('', '');
            $this->load->helper('common');
            
            $menu_data['current_class'] = $this->router->fetch_class();
            $menu_data['current_method'] = $this->router->fetch_method();
            $menu_data['sidebar_menu_list'] = $this->menu_model->get_all_menus(0, 1, $this->data['admin_group_id']);
            $this->data['menu_list'] = $this->load->view('common/menu_list', $menu_data, TRUE);

            // Check valid class and method
            if($menu_data['current_class'] != NULL && $menu_data['current_class'] != 'auth') {
                $url = $menu_data['current_class'];
                
                if($menu_data['current_method'] == 'index')
                {
                    $url.= '';
                }
                else
                {
                    $url.= '/' . $menu_data['current_method'];
                }

                
                if(!$this->menu_model->is_valid_method($this->data['admin_group_id'], $url)) {
                    js_set_message('Not enough privileges!', site_url());
                }
            }
        }
        else
        {
        	redirect(site_url());
        }
    }
}