<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {
    /**
     * Constructor for MY model
     * This method is main model extended by almost all models in this application
     * 
     * @return void
     * @author ronaldlayanto
     */
    public function __construct() 
    {
        parent::__construct();
    }

    /**
     * Method fetch_all
     * This method is used to query limited amount of data rows
     * This method is used for pagination
     * 
     * @param string $query 
     * @param int $limit 
     * @param int $offset 
     * @return array
     * @author ronaldlayanto
     */
    public function fetch_all($query, $limit, $offset) 
    {
        if(empty($limit))
        {
            $limit = 0;
        }
            
        $data['results'] = $this->db->query($query . ' LIMIT ' . $limit . ',' . $offset)->result_array();
        $data['total_rows'] = $this->db->query($query)->num_rows();
        return $data;
    }
}