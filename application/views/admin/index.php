<div class="page-content-wrapper">
	<div class="page-content">
		<!-- START PAGE HEADER -->
		<div class="page-head">
			<div class="page-title">
				<h1><?php echo $current_class; ?><small></small></h1>
			</div>
		</div>
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-circle"></i>
				<a href="<?php echo current_url();?>"><?php echo $current_class; ?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Show All Admin</a>
			</li>
		</ul>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box red-intense">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i>Show All Admins
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
							<a href="javascript:;" class="reload"></a>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Group</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									for($i=0;$i<count($list);$i++)
									{
								?>
								<tr>
									<td><?php echo stripslashes($list[$i]['admin_uname']); ?></td>
									<td>
										<?php
							                $admin_groups = $this->admin_model->get_groups_by_admin_id($list[$i]['admin_id']);
							                for($n=0;$n<count($admin_groups);$n++) {
							                    echo stripslashes($admin_groups[$n]['admin_group_name']);
							                    if($n<count($admin_groups)-1)
							                        echo ', ';
							                }
							            ?>
									</td>
									<td>
										<a href="<?php echo site_url('admin/edit_admin/' . $list[$i]['admin_id']); ?>">Edit</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('admin/delete_admin/' . $list[$i]['admin_id']); ?>" onclick="return sure();">Delete</a>
									</td>
								</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->