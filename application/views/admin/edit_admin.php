<div class="page-content-wrapper">
	<div class="page-content">
<!-- START PAGE HEADER -->
			<h3 class="page-title">
			<?php echo $current_class; ?><small></small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo current_url(); ?>"><?php echo $current_class; ?></a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Show All Admin</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable tabbable-custom boxless tabbable-reversed">
					<div class="tab-content">
						<div class="tab-pane active" id="tab_0">
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-cogs"></i>Edit Admin
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="javascript:;" class="reload"></a>
									</div>
								</div>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<?php
										echo form_open('admin/update_admin', array(
											'method' => 'post', 
											'class' => 'form-horizontal'
										));
									?>
										<div class="form-body">
											<div class="form-group">
												<label class="col-md-3 control-label">Username</label>
												<div class="col-md-4">
													<?php 
														echo form_input(array('id' => 'admin_uname', 'name' => 'admin_uname', 'disabled' => true,  'class' => 'form-control', 'value' => set_value('admin_uname', stripslashes($details['admin_uname'])))); 
													?>  <span class="error_msg"><?php echo form_error('admin_uname'); ?></span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Password</label>
												<div class="col-md-4">
													<?php 
														echo form_password(array('id' => 'admin_pwd', 'name' => 'admin_pwd', 'class' => 'form-control')); 
													?> <span class="error_msg"><?php if(form_error('admin_pwd') != null) { echo form_error('admin_pwd'); } else { echo '* Leave blank to keep using old password.'; } ?></span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Group</label>
												<div class="col-md-4">
													<?php
									                    for($i=0;$i<count($groups);$i++) 
									                    {
									                    	$flag = $this->admin_model->get_admin_group_assoc($details['admin_id'], $groups[$i]['admin_group_id']);
									                        echo form_checkbox(array('id' => 'group_id[]', 'name' => 'group_id[]', 'value' => $groups[$i]['admin_group_id'], 'style' => 'float:left;', 'checked' => $flag)) . stripslashes($groups[$i]['admin_group_name']);
									                        echo '<div style="height:3px;clear:both;"></div>';
									                    }    
									                ?>
									                <?php if(form_error('group_id[]') != null) { ?>
									                	<span class="error_msg"><?php echo form_error('group_id[]'); ?></span>
									                    <div style="height:5px;"></div>
									                <?php } ?>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Status</label>
												<div class="col-md-4">
													<?php echo form_dropdown('admin_status', array('0' => 'Unpublished', '1' => 'Published'), $details['admin_status'], 'class="form-control"'); ?>
												</div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<?php
														echo form_hidden('admin_uname', $details['admin_uname']);
														echo form_hidden('admin_id', $details['admin_id']);
														echo form_submit(array('id' => 'submit', 'name' => 'submit', 'value' => 'Update Admin', 'class' => 'btn blue'));
													?>
												</div>
											</div>
										</div>
									<?php
										echo form_close();
									?>
									<!-- END FORM-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->