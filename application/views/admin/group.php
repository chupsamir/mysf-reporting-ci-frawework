<div class="page-content-wrapper">
	<div class="page-content">
		<!-- START PAGE HEADER -->
		<div class="page-head">
			<div class="page-title">
				<h1><?php echo $current_class; ?><small></small></h1>
			</div>
		</div>
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-circle"></i>
				<a href="<?php echo current_url();?>"><?php echo $current_class; ?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Group</a>
			</li>
		</ul>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box red-intense">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i>Manage Admin Groups
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
							<a href="javascript:;" class="reload"></a>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									for($i=0;$i<count($list);$i++)
									{
								?>
								<tr>
									<td><?php echo stripslashes($list[$i]['admin_group_name']); ?></td>
									<td>
										<a href="<?php echo site_url('admin/edit_group/' . $list[$i]['admin_group_id']); ?>">Edit</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('admin/delete_group/' . $list[$i]['admin_group_id']); ?>" onclick=" return sure();">Delete</a>
									</td>
								</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->

		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable tabbable-custom boxless tabbable-reversed">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>New Group
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
								<a href="javascript:;" class="reload"></a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<?php
								echo form_open('admin/add_group', array(
									'method' => 'post',
									'class' => 'form-horizontal'
								));
							?>
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Name</label>
										<div class="col-md-4">
											<?php
												echo form_input(array('id' => 'admin_group_name', 'name' => 'admin_group_name', 'class' => 'form-control', 'value' => set_value('admin_group_name')));
											?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Privileges</label>
										<div class="col-md-4">
											<?php
												for($i=0;$i<count($menus);$i++) {
													echo form_checkbox(array('id' => 'fk_menu_id[]', 'name' => 'fk_menu_id[]', 'value' => $menus[$i]['menu_id'])) . ' <span style="font-weight:bold;margin-top:1px;">' . stripslashes($menus[$i]['menu_title']) . '</span>';
													$submenus = $this->menu_model->get_all_menus_by_parent_id($menus[$i]['menu_id']);
													if($submenus) {
														for($n=0;$n<count($submenus);$n++) {
															echo '<div style="height:5px;clear:both;"></div>';
															echo '<div style="padding-left:15px;">';
															echo form_checkbox(array('id' => 'fk_menu_id[]', 'name' => 'fk_menu_id[]', 'value' => $submenus[$n]['menu_id'])) . ' <span style="margin-top:1px;">' . stripslashes($submenus[$n]['menu_title']) . '</span>';
															echo '</div>';
															echo '<div style="height:2px;clear:both;"></div>';
														}
														echo '<div style="height:5px;clear:both;"></div>';
													} else {
														echo '<div style="height:5px;clear:both;"></div>';
													}
												}
											?>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<?php
												echo form_submit(array('id' => 'submit', 'name' => 'submit', 'value' => 'Add Group', 'class' => 'btn blue'));
											?>
										</div>
									</div>
								</div>
							<?php
								echo form_close();
							?>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->