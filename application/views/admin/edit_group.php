<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable tabbable-custom boxless tabbable-reversed">
					<div class="tab-content">
						<div class="tab-pane active" id="tab_0">
							<div class="portlet box yellow">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-cogs"></i>Edit Group
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="javascript:;" class="reload"></a>
									</div>
								</div>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<?php
										echo form_open('admin/update_group', array(
											'method' => 'post', 
											'class' => 'form-horizontal'
										));
									?>
										<div class="form-body">
											<div class="form-group">
												<label class="col-md-3 control-label">Name</label>
												<div class="col-md-4">
													<?php 
														echo form_input(array('id' => 'admin_group_name', 'name' => 'admin_group_name', 'class' => 'form-control', 'value' => set_value('admin_group_name', stripslashes($details['admin_group_name'])))); 
													?>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Privileges</label>
												<div class="col-md-4">
													<?php
										                for($i=0;$i<count($menus);$i++) 
										                {
										                	$menu_flag = $this->menu_model->get_admin_group_menu_assoc($details['admin_group_id'], $menus[$i]['menu_id']);

										                    echo form_checkbox(array('id' => 'fk_menu_id[]', 'name' => 'fk_menu_id[]', 'checked' => $menu_flag, 'value' => $menus[$i]['menu_id'])) . ' <span style="font-weight:bold;margin-top:1px;">' . stripslashes($menus[$i]['menu_title']) . '</span>';
										                    $submenus = $this->menu_model->get_all_menus_by_parent_id($menus[$i]['menu_id']);
										                    if($submenus) 
										                    {
										                        for($n=0;$n<count($submenus);$n++) 
										                        {
										                        	$submenu_flag = $this->menu_model->get_admin_group_menu_assoc($details['admin_group_id'], $submenus[$n]['menu_id']);

										                            echo '<div style="height:5px;clear:both;"></div>';
										                            echo '<div style="padding-left:15px;">';
										                            echo form_checkbox(array('id' => 'fk_menu_id[]', 'name' => 'fk_menu_id[]', 'checked' => $submenu_flag, 'value' => $submenus[$n]['menu_id'])) . ' <span style="margin-top:1px;">' . stripslashes($submenus[$n]['menu_title']) . '</span>';
										                            echo '</div>';
										                            echo '<div style="height:2px;clear:both;"></div>';
										                        }
										                        echo '<div style="height:5px;clear:both;"></div>';
										                    } else {
										                        echo '<div style="height:5px;clear:both;"></div>';
										                    }
										                }
										            ?>
												</div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<?php
														echo form_hidden('admin_group_id', $details['admin_group_id']);
														echo form_submit(array('id' => 'submit', 'name' => 'submit', 'value' => 'Update Group', 'class' => 'btn blue'));
													?>
												</div>
											</div>
										</div>
									<?php
										echo form_close();
									?>
									<!-- END FORM-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->