<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- START PAGE HEADER -->
		<div class="page-head">
			<div class="page-title">
				<h1><?php echo $current_class; ?><small></small></h1>
			</div>
		</div>
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-circle"></i>
				<a href="<?php echo current_url();?>"><?php echo $current_class; ?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">New Admin</a>
			</li>
		</ul>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable tabbable-custom boxless tabbable-reversed">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>New Menu
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
								<a href="javascript:;" class="reload"></a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<?php
								echo form_open('menu/add_menu', array(
									'method' => 'post',
									'class' => 'form-horizontal'
								));
							?>
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Name</label>
										<div class="col-md-4">
											<?php
												echo form_input(array('id' => 'menu_title', 'name' => 'menu_title', 'class' => 'form-control', 'value' => set_value('menu_title')));
											?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Type</label>
										<div class="col-md-4">
											<?php echo form_dropdown('fk_menu_type_id', $menu_types, 1, 'class="form-control"'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">URL</label>
										<div class="col-md-4">
											<?php
												echo form_input(array('id' => 'menu_url', 'name' => 'menu_url', 'class' => 'form-control', 'value' => set_value('menu_url')));
											?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-4">
											<?php echo form_dropdown('menu_status', array('0' => 'Unpublished', '1' => 'Published'), 1, 'class="form-control"'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Menu Icon</label>
										<div class="col-md-4">
											<?php echo form_input(array('id' => 'menu_icon', 'name' => 'menu_icon', 'class' => 'form-control', 'value' => set_value('menu_icon'))); ?>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<?php
												echo form_submit(array('id' => 'submit', 'name' => 'submit', 'value' => 'Add Menu', 'class' => 'btn blue'));
												echo form_hidden('menu_parent_id', $parent_id);
											?>
										</div>
									</div>
								</div>
							<?php
								echo form_close();
							?>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->