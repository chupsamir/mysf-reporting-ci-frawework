<div class="page-content-wrapper">
	<div class="page-content">
		<!-- START PAGE HEADER -->
		<div class="page-head">
			<div class="page-title">
				<h1><?php echo $current_class; ?><small></small></h1>
			</div>
		</div>
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-circle"></i>
				<a href="<?php echo current_url();?>"><?php echo $current_class; ?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#">Show All Menu</a>
			</li>
		</ul>
		<!-- END PAGE HEADER-->
		<!--
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="index.html"><?php echo current_url(); ?></a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="#"><?php echo current_url(); ?></a>
				<i class="fa fa-circle"></i>
			</li>
		</ul>
		-->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box red-intense">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i>Show All Menus
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
							<a href="javascript:;" class="reload"></a>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									for($i=0;$i<count($list);$i++)
									{
								?>
								<tr>
									<td><?php echo stripslashes($list[$i]['menu_title']); ?></td>
									<td>
										<a href="<?php echo site_url('menu/details/' . $list[$i]['menu_id']); ?>">View Details</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('menu/edit_menu/' . $list[$i]['menu_id']); ?>">Edit</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('menu/delete_menu/' . $list[$i]['menu_id']); ?>" onclick="return sure();">Delete</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('menu/move_menu/up/' . $list[$i]['menu_id']); ?>">Move Up</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('menu/move_menu/down/' . $list[$i]['menu_id']); ?>">Move Down</a>
									</td>
								</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->