<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE TABLE PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cogs"></i>Menu Details
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
							<a href="javascript:;" class="reload"></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Name</th>
										<th>Type</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo stripslashes($details['menu_title']); ?></td>
										<td><?php echo stripslashes($details['menu_type_title']); ?></td>
										<td>
											<?php 
												if($details['menu_status'] == 1)
												{
													echo 'Published';
												}
												else
												{
													echo 'Unpublished';
												}
											?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END SAMPLE TABLE PORTLET-->

				<?php
				    if($details['menu_parent_id'] != null && $details['menu_parent_id'] != 0)
				    {
				    	$back_url = site_url('menu/details/' . $details['menu_parent_id']);
				    }
				    else
				    {
				    	$back_url = site_url('menu');
				    }
				?>
				<a href="<?php echo site_url('menu/new_menu/' . $details['menu_id']); ?>">Add New Submenu</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo $back_url; ?>">Back to Previous Page</a>
				<br /><br />

				<!-- BEGIN SAMPLE TABLE PORTLET-->
				<div class="portlet box purple">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cogs"></i>Submenu Details
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
							<a href="javascript:;" class="reload"></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Name</th>
										<th>Type</th>
										<th>URL</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
										for($i=0;$i<count($submenus);$i++)
										{
									?>
									<tr>
										<td><?php echo stripslashes($submenus[$i]['menu_title']); ?></td>
										<td><?php echo stripslashes($submenus[$i]['menu_type_title']); ?></td>
										<td><?php echo stripslashes($submenus[$i]['menu_url']); ?></td>
										<td>
											<?php 
												if($submenus[$i]['menu_status'] == 1)
												{
													echo 'Published';
												}
												else
												{
													echo 'Unpublished';
												}
											?>
										</td>
										<td>
											<a href="<?php echo site_url('menu/edit_menu/' . $submenus[$i]['menu_id'] . '/' . $submenus[$i]['menu_parent_id']); ?>">Edit</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('menu/delete_menu/' . $submenus[$i]['menu_id'] . '/' . $submenus[$i]['menu_parent_id']); ?>" onclick="return sure();">Delete</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('menu/move_menu/up/' . $submenus[$i]['menu_id'] . '/' . $submenus[$i]['menu_parent_id']); ?>">Move Up</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('menu/move_menu/down/' . $submenus[$i]['menu_id'] . '/' . $submenus[$i]['menu_parent_id']); ?>">Move Down</a>
										</td>
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END SAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->