<div class="page-content-wrapper">
    <div class="page-content">
        <!-- START PAGE HEADER -->
        <div class="page-head">
            <div class="page-title">
                <h1><?php echo $current_class; ?><small></small></h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-circle"></i>
                <a href="<?php echo current_url();?>"><?php echo $current_class; ?></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Daily Average Data Transfered</a>
            </li>
        </ul>
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="row">
            <div class="col-md-12">
                <div id="targetingrange" class="btn default pull-right">
                    <i class="fa fa-calendar"></i>
                    &nbsp; <span>
                                                    </span>
                    <b class="fa fa-angle-down"></b>
                    <input type="hidden" name="to" id="to" value="">
                    <input type="hidden" name="from" id="from" value="">
                </div>
            </div>
        </div>
        <div class="margin-bottom-10"></div>
        <!-- END PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Daily Average</span>
                            <span class="caption-helper">Data Transferred/Received</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="fullscreen">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="app_data_avg" class="chart" style="height: 300px;">
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-comments"></i>Summary Campaigns
                                </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse">
                                    </a>
                                    </a>
                                    <a href="javascript:;" class="reload">
                                    </a>
                                    <a href="javascript:;" class="remove">
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table id="app_avg" class="table table-bordered table-hover">
                                    <thead>
                                    <tr class="warning">
                                        <th>
                                            Application
                                        </th>
                                        <th>
                                            Data Transferred (in Kb)
                                        </th>
                                        <th>
                                            Data Received (in Kb)
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
            </div>
        </div>