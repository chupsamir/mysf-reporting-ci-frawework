<div class="page-content-wrapper">
    <div class="page-content">
        <!-- START PAGE HEADER -->
        <div class="page-head">
            <div class="page-title">
                <h1>Application Data<small> Transfer & Received</small></h1>
            </div>
        </div>
        <div class="margin-bottom-15"></div>
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="row">
            <div class="col-md-12">
                <div id="targetingrange" class="btn default pull-right">
                    <i class="fa fa-calendar"></i>
                    &nbsp; <span>
                                                    </span>
                    <b class="fa fa-angle-down"></b>
                    <input type="hidden" name="to" id="to" value="">
                    <input type="hidden" name="from" id="from" value="">
                </div>
            </div>
        </div>
        <div class="margin-bottom-10"></div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Applications</span>
                            <span class="caption-helper">Data Transferred/Received</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="fullscreen">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_data_transfered" class="chart" style="height: 600px;">
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cube"></i>Application Data Transfer
                                </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse">
                                    </a>
                                    </a>
                                    <a href="javascript:;" class="reload">
                                    </a>
                                    <a href="javascript:;" class="remove">
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table id="app_data" class="table table-bordered table-hover">
                                    <thead>
                                    <tr class="warning">
                                        <th>
                                            Application
                                        </th>
                                        <th>
                                            Data Transferred (in Kb)
                                        </th>
                                        <th>
                                            Data Received (in Kb)
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
            </div>
        </div>