<div class="page-content-wrapper">
    <div class="page-content">
        <!-- START PAGE HEADER -->
        <div class="page-head">
            <div class="page-title">
                <h1>Application Duration<small></small></h1>
            </div>
        </div>
        <div class="margin-bottom-15"></div>
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="row">
            <div class="col-md-12">
                <div id="targetingrange" class="btn default pull-right">
                    <i class="fa fa-calendar"></i>
                    &nbsp; <span>
                                                    </span>
                    <b class="fa fa-angle-down"></b>
                    <input type="hidden" name="to" id="to" value="">
                    <input type="hidden" name="from" id="from" value="">
                </div>
            </div>
        </div>
        <div class="margin-bottom-10"></div>
        <!-- END PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Application Duration</span>
                            <span class="caption-helper">Duration</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="fullscreen">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_duration" class="chart" style="height: 300px;">
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-history"></i>Application Duration
                                </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse">
                                    </a>
                                    </a>
                                    <a href="javascript:;" class="reload">
                                    </a>
                                    <a href="javascript:;" class="remove">
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table id="duration" class="table table-bordered table-hover">
                                    <thead>
                                    <tr class="warning">
                                        <th>
                                            Application
                                        </th>
                                        <th>
                                            Duration (in Minutes)
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>