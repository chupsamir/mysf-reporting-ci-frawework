<div class="page-content-wrapper">
    <div class="page-content">
        <!-- START PAGE HEADER -->
        <div class="page-head">
            <div class="page-title">
                <h1>Monthly Revenue<small> Statistic & Reports</small></h1>
            </div>
        </div>
        <div class="margin-bottom-15"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="margin-bottom-10">
                    <select id="currency" class="bs-select form-control input-small" data-style="red">
                        <option value="IDR">IDR</option>
                        <option value="USD">USD</option>
                    </select>
                    <select id="price_type" class="bs-select form-control input-small" data-style="blue">
                        <option value="">Price Type</option>
                        <option value="CPC">CPC</option>
                        <option value="CPA">CPA</option>
                        <option value="CPM">CPM</option>
                        <option value="CPI">CPI</option>
                    </select>
                    <select id="group_id" class="bs-select form-control input-small" data-style="green">
                        <option>Client Group</option>
                        <option>Campaign A</option>
                        <option>Campaign B</option>
                        <option>Campaign C</option>
                    </select>
                    <select id="campaign_type" class="bs-select form-control input-small" data-style="yellow">
                        <option data-icon="fa-filter icon-success" value="">Campaign Type</option>
                        <option data-icon="fa-file-audio-o  icon-success" value="3">Audio</option>
                        <option data-icon="fa-cube icon-warning" value="5">APK</option>
                        <option data-icon="fa-file-video-o icon-warning" value="7">Video</option>
                        <option data-icon="fa-file-photo-o icon-warning" value="9">Image</option>
                        <option data-icon="fa-file-text-o icon-warning" value="10">Text</option>
                        <option data-icon="fa-mail-forward icon-warning" value="14">Shortcut Icon</option>
                    </select>
                    <div id="financerange" class="btn default pull-right">
                        <i class="fa fa-calendar"></i>
                        &nbsp; <span>
                                                    </span>
                        <b class="fa fa-angle-down"></b>
                        <input type="hidden" name="to" id="to" value="">
                        <input type="hidden" name="from" id="from" value="">
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Monthly Reporting</span>
                            <span class="caption-helper"></span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="fullscreen">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_monthly" class="chart" style="height: 400px;">
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->

        <!-- END PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-history"></i>Summary Campaigns
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                            <table id="summary_type" class="table table-bordered table-hover">
                                <thead>
                                <tr class="warning">
                                    <th>
                                        Type
                                    </th>
                                    <th>
                                        Price Type
                                    </th>
                                    <th>
                                        Total Price
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                    <th>
                                        Total Budget
                                    </th>
                                    <th class="sum">
                                        Monthly Revenue
                                    </th>
                                    <th class="sum">
                                        SF Share
                                    </th>
                                    <th class="sum">
                                        Amk Share
                                    </th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th colspan="5">Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>

                            </table>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
                <div id="text" class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file-text"></i>TEXT
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="summary_text" class="table table-bordered table-hover">
                            <thead>
                            <tr class="warning">
                                <th>
                                    Campaign Name
                                </th>
                                <th>
                                    Price Type
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Action
                                </th>
                                <th>
                                    Budget
                                </th>
                                <th>
                                    Scaps
                                </th>
                                <th>
                                    Campaign Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    SF Share
                                </th>
                                <th>
                                    AMK Share
                                </th>
                                <th class="sum">
                                    Monthly Revenue
                                </th>
                                <th class="sum">
                                    AMK Revenue
                                </th>
                                <th class="sum">
                                    SF Revenue
                                </th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="10">Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="apk" class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cube"></i>APK
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="summary_apk" class="table table-bordered table-hover">
                            <thead>
                            <tr class="warning">
                                <th>
                                    Campaign Name
                                </th>
                                <th>
                                    Price Type
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Action
                                </th>
                                <th>
                                    Budget
                                </th>
                                <th>
                                    Scaps
                                </th>
                                <th>
                                    Campaign Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    SF Share
                                </th>
                                <th>
                                    AMK Share
                                </th>
                                <th class="sum">
                                    Monthly Revenue
                                </th>
                                <th class="sum">
                                    AMK Revenue
                                </th>
                                <th class="sum">
                                    SF Revenue
                                </th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="10">Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="image" class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file-image-o"></i>IMAGE
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="summary_image" class="table table-bordered table-hover">
                            <thead>
                            <tr class="warning">
                                <th>
                                    Campaign Name
                                </th>
                                <th>
                                    Price Type
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Action
                                </th>
                                <th>
                                    Budget
                                </th>
                                <th>
                                    Scaps
                                </th>
                                <th>
                                    Campaign Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    SF Share
                                </th>
                                <th>
                                    AMK Share
                                </th>
                                <th class="sum">
                                    Monthly Revenue
                                </th>
                                <th class="sum">
                                    AMK Revenue
                                </th>
                                <th class="sum">
                                    SF Revenue
                                </th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="10">Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="audio" class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file-audio-o"></i>AUDIO
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="summary_audio" class="table table-bordered table-hover">
                            <thead>
                            <tr class="warning">
                                <th>
                                    Campaign Name
                                </th>
                                <th>
                                    Price Type
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Action
                                </th>
                                <th>
                                    Budget
                                </th>
                                <th>
                                    Scaps
                                </th>
                                <th>
                                    Campaign Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    SF Share
                                </th>
                                <th>
                                    AMK Share
                                </th>
                                <th class="sum">
                                    Monthly Revenue
                                </th>
                                <th class="sum">
                                    AMK Revenue
                                </th>
                                <th class="sum">
                                    SF Revenue
                                </th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="10">Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="video" class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file-video-o"></i>VIDEO
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="summary_video" class="table table-bordered table-hover">
                            <thead>
                            <tr class="warning">
                                <th>
                                    Campaign Name
                                </th>
                                <th>
                                    Price Type
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Action
                                </th>
                                <th>
                                    Budget
                                </th>
                                <th>
                                    Scaps
                                </th>
                                <th>
                                    Campaign Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    SF Share
                                </th>
                                <th>
                                    AMK Share
                                </th>
                                <th class="sum">
                                    Monthly Revenue
                                </th>
                                <th class="sum">
                                    AMK Revenue
                                </th>
                                <th class="sum">
                                    SF Revenue
                                </th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="10">Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="shortcut" class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-mail-reply"></i>SHORTCUT ICON
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="summary_shortcut" class="table table-bordered table-hover">
                            <thead>
                            <tr class="warning">
                                <th>
                                    Campaign Name
                                </th>
                                <th>
                                    Price Type
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Action
                                </th>
                                <th>
                                    Budget
                                </th>
                                <th>
                                    Scaps
                                </th>
                                <th>
                                    Campaign Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    SF Share
                                </th>
                                <th>
                                    AMK Share
                                </th>
                                <th class="sum">
                                    Monthly Revenue
                                </th>
                                <th class="sum">
                                    AMK Revenue
                                </th>
                                <th class="sum">
                                    SF Revenue
                                </th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="10">Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>