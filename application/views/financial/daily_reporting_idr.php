<div class="page-content-wrapper">
    <div class="page-content">
        <!-- START PAGE HEADER -->
        <div class="page-head">
            <div class="page-title">
                <h1>IDR Revenue<small> Daily Reporting</small></h1>
            </div>
        </div>
        <div class="margin-bottom-15"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="margin-bottom-10">
                    <select id="price_type" class="bs-select form-control input-small" data-style="blue">
                        <option value="">Price Type</option>
                        <option value="CPC">CPC</option>
                        <option value="CPA">CPA</option>
                        <option value="CPM">CPM</option>
                        <option value="CPI">CPI</option>
                    </select>
                    <select id="group_ud" class="bs-select form-control input-small" data-style="green">
                        <option>Group</option>
                        <option>Campaign A</option>
                        <option>Campaign B</option>
                        <option>Campaign C</option>
                    </select>
                    <div id="financerange" class="btn default pull-right">
                        <i class="fa fa-calendar"></i>
                        &nbsp; <span>
                                                    </span>
                        <b class="fa fa-angle-down"></b>
                        <input type="hidden" name="to" id="to" value="">
                        <input type="hidden" name="from" id="from" value="">
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Daily Reporting</span>
                            <span class="caption-helper">duration on value axis</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="fullscreen">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_daily_IDR" class="chart" style="height: 400px;">
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-money"></i> IDR Daily Revenue
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-bordered table-hover" id="daily_idr">
                            <thead>
                            <tr class="warning">
                                <th rowspan="2">
                                    Date
                                </th>
                                <th rowspan="2">
                                    Inventory
                                </th >
                                <th rowspan="2">
                                    Price
                                </th>
                                <th rowspan="2" class="sum">
                                    Served
                                </th>
                                <th rowspan="2" class="sum">
                                    Received
                                </th>
                                <th rowspan="2">
                                    Received Percentage
                                </th>
                                <th rowspan="2" class="sum">
                                    Display/Impression
                                </th>
                                <th rowspan="2">
                                    Display Percentage
                                </th>
                                <th rowspan="2" class="sum">
                                    Clicks
                                </th>
                                <th rowspan="2">
                                    CTR
                                </th>
                                <th rowspan="2" class="sum">
                                    Total Revenue
                                </th>
                                <th colspan="2">
                                    Revenue Calculation
                                </th>
                            </tr>
                            <tr class="warning">
                                <th class="sum">
                                    AMK Share
                                </th>
                                <th class="sum">
                                    SF Share
                                </th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="3">Total:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>