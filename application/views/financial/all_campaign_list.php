<div class="page-content-wrapper">
    <div class="page-content">
        <!-- START PAGE HEADER -->
        <div class="page-head">
            <div class="page-title">
                <h1>List All Campaign<small></small></h1>
            </div>
        </div>
        <div class="margin-bottom-15"></div>
        <div class="row">
            <div class="col-md-12">
                    <div id="financerange" class="btn default pull-right">
                        <i class="fa fa-calendar"></i>
                        &nbsp; <span>
                                                    </span>
                        <b class="fa fa-angle-down"></b>
                        <input type="hidden" name="to" id="to" value="">
                        <input type="hidden" name="from" id="from" value="">
                    </div>
            </div>
        </div>
        <div class="margin-bottom-10"></div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pin"></i>Summary Campaigns
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="campaign" class="table table-bordered table-hover">
                            <thead>
                            <tr class="warning">
                                <th>
                                    Campaign Name
                                </th>
                                <th>
                                    Created Date
                                </th>
                                <th>
                                    Start Date
                                </th>
                                <th>
                                    End Date
                                </th>
                                <th>
                                    Content Type
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>