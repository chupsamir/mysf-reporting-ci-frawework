<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			
			<?php 
				for($i=0;$i<count($sidebar_menu_list);$i++) 
				{
					$exp_url = explode('/', $sidebar_menu_list[$i]['menu_url']);
					
					if(isset($exp_url[0]))
					{
						$class = $exp_url[0];
					}
					
					if(isset($exp_url[1]))
					{
						$method = $exp_url[1];
					}

			?>
			
			<!-- sidebar menu begin -->
			<li class="<?php if(isset($current_class) && ($class == $current_class)) echo 'active open'; ?>">
					<a href="javascript:;">
					<i class="<?php echo stripslashes($sidebar_menu_list[$i]['menu_icon']) ?>"></i>
					<span class="title"><?php echo stripslashes($sidebar_menu_list[$i]['menu_title']); ?></span>
					</a>
						<ul class="sub-menu">
						<?php
				            $menu_childs = $this->menu_model->get_all_menus($sidebar_menu_list[$i]['menu_id'], 1, $this->data['admin_group_id']);

				            for($n=0;$n<count($menu_childs);$n++)
				            {
				        ?>
				        	<li class="<?php if(current_url() == site_url($menu_childs[$n]['menu_url'])) echo 'active'; ?>">
								<a href="<?php echo site_url($menu_childs[$n]['menu_url']); ?>">
									<i class="<?php echo stripslashes($menu_childs[$n]['menu_icon'])  ?>"></i>
									<?php echo stripslashes($menu_childs[$n]['menu_title']); ?>
								</a>
							</li>
				        <?php
				            }
				        ?>
					</ul>
				</li>
					<?php 
				} 
			?>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>