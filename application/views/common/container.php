<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title><?php if(isset($page_title)) echo $page_title; else echo 'MYSF Reporting Page'; ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php
		echo $this->load->view('common/load_style.php', $this->data, TRUE);
	?>
	<!-- END GLOBAL MANDATORY STYLES -->

	<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
<?php
	echo $this->load->view('common/header.php', $this->data, TRUE);
?>
<!-- END HEADER -->
<div class="clearfix">
</div>

<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php
	//BEGIN SIDEBAR
	echo $this->load->view('common/sidebar', $this->data, TRUE);
	//END SIDEBAR

	//BEGIN CONTENT
	if(isset($html))
	{
		echo $html;
	}
	//END CONTENT
	?>
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<?php
	echo $this->load->view('common/footer.php', $this->data, TRUE);
?>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS -->
<?php
	echo $this->load->view('common/load_scripts.php', $this->data, TRUE);
?>
<!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>