<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<?php 
				for($i=0;$i<count($sidebar_menu_list);$i++) 
				{
					$exp_url = explode('/', $sidebar_menu_list[$i]['menu_url']);
					
					if(isset($exp_url[0]))
					{
						$class = $exp_url[0];
					}
					
					if(isset($exp_url[1]))
					{
						$method = $exp_url[1];
					}
			?>
				<li<?php if(isset($current_class) && ($class == $current_class)) echo ' class="start active open"'; ?>>
					<a href="javascript:;">
						<i class="<?php 
								if($class == 'dashboard') {
										echo 'icon-home';
								} else if ($class == 'admin') {
										echo 'icon-pencil';
								} else if ($class == 'menu') {
										echo 'icon-tag';
								} else if ($class == 'member') {
										echo 'icon-user';
								} else if ($class == 'transaction') {
										echo 'icon-present';
								} else if ($class == 'product') {
										echo 'icon-handbag';
								} else {
										echo 'icon-doc';
								} ?>"></i>
						<span class="title"><?php echo stripslashes($sidebar_menu_list[$i]['menu_title']); ?></span>
						<span class="selected"></span>
						<span class="arrow open"></span>
					</a>
					<ul class="sub-menu">
						<?php
				            $menu_childs = $this->menu_model->get_all_menus($sidebar_menu_list[$i]['menu_id'], 1, $this->data['admin_group_id']);
				            for($n=0;$n<count($menu_childs);$n++) 
				            {
				        ?>
				        	<li>
								<a href="<?php echo site_url($menu_childs[$n]['menu_url']); ?>">
									<?php echo stripslashes($menu_childs[$n]['menu_title']); ?>
								</a>
							</li>
				        <?php
				            }
				        ?>
					</ul>
				</li>
			<?php 
				} 
			?>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
<!-- END SIDEBAR -->
