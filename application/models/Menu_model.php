<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends MY_Model {
    /**
     * Constructor for Menu model
     * 
     * @return void
     */
    public function __construct() 
    {
        parent::__construct();
    }

    /**
     * Method add
     * Used to add new menu into database
     * 
     * @param array $data 
     * @return boolean
     */
    public function add($data)
    {
        return $this->db->insert('menus', $data);
    }

    /**
     * Method update
     * Used to update menu details in database
     * 
     * @param int $id 
     * @param array $data 
     * @return boolean
     */
    public function update($id, $data)
    {
        $this->db->where('menu_id', $id);
        return $this->db->update('menus', $data);
    }

    /**
     * Method delete
     * Used to delete menu from database
     * 
     * @param int $id 
     * @return boolean
     */
    public function delete($id)
    {
        $this->db->where('menu_id', $id);
        return $this->db->delete('menus');
    }

    /**
     * Method add_admin_group_menu_assoc
     * Used to add admin group menu associations data
     * 
     * @param array $data 
     * @return boolean
     */
    public function add_admin_group_menu_assoc($data)
    {
        return $this->db->insert('admin_group_menu_assocs', $data);
    }
    
    /**
     * Method is_valid_method
     * Check menu validation for admin group based on url
     * 
     * @param int $admin_group_id 
     * @param string $url 
     * @return boolean
     * note : there is a bug on escape_url on codeigniter 3
     * by @amir 20160316
     */
    public function is_valid_method($admin_group_id, $url)

    {

        $rs = $this->db->query('SELECT COUNT(*) AS total FROM menus a, admin_group_menu_assocs b
                                WHERE a.menu_id = b.fk_menu_id
                                AND a.menu_status = 1 
                                AND b.fk_admin_group_id = ' . $this->db->escape($admin_group_id) . '
                                AND a.menu_url LIKE \'' . $url . '%\'')->row_array();

        
        if($rs['total'] > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    /**
     * Method get_all_menu_types
     * Used to get all menu types from database
     * 
     * @return array
     */
    public function get_all_menu_types() 
    {
        return $this->db->query('SELECT * FROM menu_types ORDER BY menu_type_title ASC')->result_array();
    }
    
    /**
     * Method get_menu
     * Used to get menu details from database by id
     * 
     * @param int $id 
     * @return array
     */
    public function get_menu($id) 
    {
        return $this->db->query('SELECT a.*, b.menu_type_title 
                                 FROM menus a, menu_types b
                                 WHERE a.fk_menu_type_id = b.menu_type_id 
                                 AND a.menu_id = ' . $this->db->escape($id))->row_array();
    }
    
    /**
     * Method get_all_menus_by_parent_id
     * Used to get all menus by parent id
     * 
     * @param int $parent_id 
     * @param int $menu_type 
     * @param string $keyword 
     * @return array
     */
    public function get_all_menus_by_parent_id($parent_id, $menu_type = NULL, $keyword = NULL) 
    {
        $query = 'SELECT * FROM menus a, menu_types b 
                  WHERE a.fk_menu_type_id = b.menu_type_id 
                  AND a.menu_parent_id = ' . $this->db->escape($parent_id);
        
        if(isset($menu_type) && $menu_type != NULL)
        {
            $query.= ' AND a.fk_menu_type_id = ' . $this->db->escape($menu_type);
        }
            
        if(isset($keyword) && $keyword != NULL)
        {
            $query.= ' AND a.menu_title LIKE \'%' . $this->db->escape_like_str($keyword) . '%\'';
        }     
        
        $query.= ' ORDER BY a.menu_position ASC';
        
        return $this->db->query($query)->result_array();
    }
    
    /**
     * Method get_all_non_root_menus_by_parent_id
     * Used to get all non root menus by parent id
     * 
     * @param int $parent_id 
     * @param int $menu_type 
     * @param string $keyword 
     * @return array
     */
    public function get_all_non_root_menus_by_parent_id($parent_id, $menu_type = NULL, $keyword = NULL) 
    {
        $query = 'SELECT * FROM menus a, menu_types b 
                  WHERE a.fk_menu_type_id = b.menu_type_id
                  AND a.menu_root != 1 
                  AND a.menu_parent_id = ' . $this->db->escape($parent_id);
        
        if(isset($menu_type) && $menu_type != NULL)
        {
            $query.= ' AND a.fk_menu_type_id = ' . $this->db->escape($menu_type);
        }
        
        if(isset($keyword) && $keyword != NULL)        
        {
            $query.= ' AND a.menu_title LIKE \'%' . $this->db->escape_like_str($keyword) . '%\'';
        }
        
        $query.= ' ORDER BY a.menu_position ASC';
        
        return $this->db->query($query)->result_array();
    }
    
    /**
     * Method get_all_menus
     * Used to get all menus with some conditions
     * 
     * @param int $parent_id 
     * @param int $menu_type 
     * @param int $admin_group_id 
     * @param string $keyword 
     * @return array
     */
    public function get_all_menus($parent_id, $menu_type = NULL, $admin_group_id, $keyword = NULL) 
    {
        $query = 'SELECT * FROM menus a, admin_group_menu_assocs b, menu_types c 
                  WHERE a.menu_id = b.fk_menu_id
                  AND a.fk_menu_type_id = c.menu_type_id 
                  AND a.menu_parent_id = ' . $this->db->escape($parent_id);
        
        if(isset($menu_type) && $menu_type != NULL)
        {
            $query.= ' AND a.fk_menu_type_id = ' . $this->db->escape($menu_type);
        }
            
        $query.= ' AND b.fk_admin_group_id = ' . $this->db->escape($admin_group_id);
        
        if(isset($keyword) && $keyword != NULL)
        {
            $query.= ' AND a.menu_title LIKE \'%' . $this->db->escape_like_str($keyword) . '%\'';
        }
        
        $query.= ' ORDER BY a.fk_menu_type_id, a.menu_position ASC';
        
        return $this->db->query($query)->result_array();
    }
    
    /**
     * Method get_last_position
     * Used to get menu last position
     * 
     * @param int $parent_id 
     * @return array
     */
    public function get_last_position($parent_id = NULL) 
    {
        if(!isset($parent_id) || $parent_id == NULL)
        {
            $parent_id = 0;
        }
        
        $rs = $this->db->query('SELECT menu_position FROM menus
                                WHERE menu_parent_id = ' . $this->db->escape($parent_id) . '
                                ORDER BY menu_position DESC')->row_array();
        
        if($rs)
        {
            return $rs['menu_position']; 
        }
        else
        {
            return 0;
        }
    }
    
    /**
     * Method get_next_position
     * Used to get menu next available position
     * @param int $pos 
     * @param int $parent_id 
     * @return array
     */
    public function get_next_position($pos, $parent_id) 
    {
        return $this->db->query('SELECT * FROM menus 
                                 WHERE menu_position > ' . $this->db->escape($pos) . ' 
                                 AND menu_parent_id = ' . $this->db->escape($parent_id))->row_array();
    }
    
    /**
     * Method get_previous_position
     * Used to get menu previous position
     * 
     * @param int $pos 
     * @param int $parent_id 
     * @return array
     */
    public function get_previous_position($pos, $parent_id) 
    {
        return $this->db->query('SELECT * FROM menus 
                                 WHERE menu_position < ' . $this->db->escape($pos) . ' 
                                 AND menu_parent_id = ' . $this->db->escape($parent_id) . ' 
                                 ORDER BY menu_position DESC')->row_array();
    }
    
    /**
     * Method update_position
     * Used to update menu position
     * 
     * @param string $pos 
     * @param int $id 
     * @return boolean
     */
    public function update_position($pos, $id) 
    {
        $first = $this->get_menu($id);

        if($pos == 'down')
        {
            $second = $this->get_next_position($first['menu_position'], $first['menu_parent_id']);
        }
        else
        {
            $second = $this->get_previous_position($first['menu_position'], $first['menu_parent_id']);
        }

        $pos1 = $first['menu_position'];
        $pos2 = $second['menu_position'];

        if($second != NULL) 
        {
            $this->db->query('UPDATE menus SET menu_position = ' . $this->db->escape($pos1) . ' 
                              WHERE menu_id = ' . $this->db->escape($second['menu_id']));
            $this->db->query('UPDATE menus SET menu_position = ' . $this->db->escape($pos2) . ' 
                              WHERE menu_id = ' . $this->db->escape($first['menu_id']));
        }

        return TRUE;
    }
    
    /**
     * Method get_admin_group_menu_assoc
     * Used to get admin group menu associations data
     * 
     * @param int $admin_group_id 
     * @param int $menu_id 
     * @return boolean
     */
    public function get_admin_group_menu_assoc($admin_group_id, $menu_id) 
    {
        $count = $this->db->query('SELECT * FROM admin_group_menu_assocs
                                   WHERE fk_admin_group_id = ' . $this->db->escape($admin_group_id) . '
                                   AND fk_menu_id = ' . $this->db->escape($menu_id))->num_rows();
        
        if($count > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    /**
     * Method delete_admin_group_menu_assoc_by_group_id
     * Used to delete admin group menu associations by admin group id
     * 
     * @param int $admin_group_id 
     * @return boolean
     */
    public function delete_admin_group_menu_assoc_by_group_id($admin_group_id) 
    {
        return $this->db->query('DELETE FROM admin_group_menu_assocs WHERE fk_admin_group_id = ' . $this->db->escape($admin_group_id));
    }
    
    /**
     * Method delete_admin_group_menu_assoc_by_menu_id
     * Used to delete admin group menu associations by menu id
     * 
     * @param int $menu_id 
     * @return boolean
     */
    public function delete_admin_group_menu_assoc_by_menu_id($menu_id) 
    {
        return $this->db->query('DELETE FROM admin_group_menu_assocs WHERE fk_menu_id = ' . $this->db->escape($menu_id));
    }
}
