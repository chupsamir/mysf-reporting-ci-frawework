<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends MY_Model {
    /**
     * Constructor for Auth model
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Method check_admin_authentication
     * Used to get admin details by username and password
     * 
     * @param string $username 
     * @param string $password 
     * @return array
     */
    public function check_admin_authentication($username, $password) {
        $rs = $this->db->query('SELECT a.admin_id, a.admin_uname, b.admin_group_id, b.admin_group_name 
                                FROM admins a, admin_groups b, admin_group_assocs c 
                                WHERE a.admin_id = c.fk_admin_id 
                                AND b.admin_group_id = c.fk_admin_group_id 
                                AND a.admin_status = 1 
                                AND a.admin_uname = ' . $this->db->escape($username) . ' 
                                AND a.admin_pwd = ' . $this->db->escape($password));
        return $rs->row_array();
    }

    /**
     * Method authenticate_admin
     * Used to authenticate admin by username and password
     * 
     * @param array $input 
     * @return boolean
     */
    public function authenticate_admin($input) {
        $username = $input['username'];
        $password = $input['password'];
        $auth = $this->check_admin_authentication($username, $password);

        if($auth != null) {
            $session_data = array('admin_id' => $auth['admin_id'], 
                                  'admin_uname' => $auth['admin_uname'], 
                                  'admin_group_id' => $auth['admin_group_id']);
            $this->session->set_userdata('mc_admin_session', $session_data);
            $this->session->set_userdata('editor_document_base_url', EDITOR_DOCUMENT_BASE_URL);
            $this->session->set_userdata('editor_rootpath', EDITOR_ROOTPATH);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method is_logged
     * Used to check if admin already logged in or not
     * 
     * @return boolean
     */
    public function is_logged() {
        if($this->session->userdata('mc_admin_session')) {
            return true;
        } else {
            return false;
        }
    }
}