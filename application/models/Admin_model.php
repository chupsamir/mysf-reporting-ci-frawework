<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends MY_Model {
    /**
     * Constructor for Admin model
     * 
     * @return void
     */
    public function __construct() 
    {
        parent::__construct();
    }

    /**
     * Method add_group
     * Used to add new admin group into database
     * 
     * @param array $data 
     * @return boolean
     */
    public function add_group($data)
    {
        if($this->db->insert('admin_groups', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Method update_group
     * Used to update admin group details in database
     * 
     * @param int $id 
     * @param array $data 
     * @return boolean
     */
    public function update_group($id, $data)
    {
        $this->db->where('admin_group_id', $id);
        return $this->db->update('admin_groups', $data);
    }

    /**
     * Method delete_group
     * Used to delete admin group from database
     * 
     * @param int $id 
     * @return boolean
     */
    public function delete_group($id)
    {
        $this->db->where('admin_group_id', $id);
        return $this->db->delete('admin_groups');
    }
    
    /**
     * Method get_group
     * Used to get admin group by id
     * 
     * @param int $id 
     * @return array
     */
    public function get_group($id) 
    {
        return $this->db->query('SELECT * FROM admin_groups WHERE admin_group_id = ' . $this->db->escape($id))->row_array();
    }
    
    /**
     * Method get_groups_by_admin_id
     * Used to get admin groups by admin id
     * 
     * @param int $admin_id 
     * @return array
     */
    public function get_groups_by_admin_id($admin_id) 
    {
        return $this->db->query('SELECT a.* FROM admin_groups a, admin_group_assocs b
                                 WHERE a.admin_group_id = b.fk_admin_group_id
                                 AND b.fk_admin_id = ' . $this->db->escape($admin_id) . '
                                 ORDER BY a.admin_group_name ASC')->result_array();
    }
    
    /**
     * Method get_all_groups
     * Used to get all admin groups
     * 
     * @return array
     */
    public function get_all_groups() 
    {
        return $this->db->query('SELECT * FROM admin_groups ORDER BY admin_group_name ASC')->result_array();
    }
    
    /**
     * Method get_all_non_root_groups
     * Used to get all non root admin groups
     * 
     * @return array
     */
    public function get_all_non_root_groups() 
    {
        return $this->db->query('SELECT * FROM admin_groups
                                 WHERE admin_group_id != 1
                                 ORDER BY admin_group_name ASC')->result_array();
    }
    
    /**
     * Method get_admin_group_assoc
     * Used to get admin group associations
     * 
     * @param int $admin_id 
     * @param int $admin_group_id 
     * @return boolean
     */
    public function get_admin_group_assoc($admin_id, $admin_group_id) 
    {
        $count = $this->db->query('SELECT * FROM admin_group_assocs
                                   WHERE fk_admin_id = ' . $this->db->escape($admin_id) . '
                                   AND fk_admin_group_id = ' . $this->db->escape($admin_group_id))->num_rows();
        
        if($count > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    /**
     * Method delete_admin_group_assoc_by_admin_id
     * Used to delete admin group associations by admin id 
     * 
     * @param int $admin_id 
     * @return array
     */
    public function delete_admin_group_assoc_by_admin_id($admin_id) 
    {
        return $this->db->query('DELETE FROM admin_group_assocs WHERE fk_admin_id = ' . $this->db->escape($admin_id));
    }
    
    /**
     * Method delete_admin_group_assoc_by_group_id
     * Used to delete admin group associations by group id
     * 
     * @param int $admin_group_id 
     * @return array
     */
    public function delete_admin_group_assoc_by_group_id($admin_group_id) 
    {
        return $this->db->query('DELETE FROM admin_group_assocs WHERE fk_admin_group_id = ' . $this->db->escape($admin_group_id));
    }

    /**
     * Method add
     * Used to add new admin into database
     * 
     * @param array $data 
     * @return boolean - if false
     * @return int - if true
     */
    public function add($data)
    {
        if($this->db->insert('admins', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Method update
     * Used to update admin details in database
     * 
     * @param int $id 
     * @param array $data 
     * @return boolean
     */
    public function update($id, $data)
    {
        $this->db->where('admin_id', $id);
        return $this->db->update('admins', $data);
    }

    /**
     * Method delete
     * Used to delete admin from database
     * 
     * @param int $id 
     * @return boolean
     */
    public function delete($id)
    {
        $this->db->where('admin_id', $id);
        return $this->db->delete('admins');
    }

    /**
     * Method get_admin
     * Used to get admin details by id
     * 
     * @param int $id 
     * @return array
     */
    public function get_admin($id) 
    {
        return $this->db->query('SELECT * FROM admins WHERE admin_id = ' . $this->db->escape($id))->row_array();
    }
    
    /**
     * Method get_all_admins
     * Used to get all admins
     * 
     * @return array
     */
    public function get_all_admins() 
    {
        return $this->db->query('SELECT * FROM admins a, admin_groups b, admin_group_assocs c 
                                 WHERE a.admin_id = c.fk_admin_id 
                                 AND b.admin_group_id = c.fk_admin_group_id
                                 GROUP BY a.admin_id 
                                 ORDER BY b.admin_group_name ASC, a.admin_uname ASC')->result_array();
    }
    
    /**
     * Method get_all_non_root_admins
     * Used to get all non root admins
     * 
     * @return array
     */
    public function get_all_non_root_admins() 
    {
        return $this->db->query('SELECT * FROM admins a, admin_groups b, admin_group_assocs c 
                                 WHERE a.admin_id = c.fk_admin_id
                                 AND b.admin_group_id = c.fk_admin_group_id
                                 AND b.admin_group_id != 1
                                 GROUP BY a.admin_id 
                                 ORDER BY b.admin_group_name ASC, a.admin_uname ASC')->result_array();
    }

    /**
     * Method add_admin_group_assoc
     * Used to add admin group associations data
     * 
     * @param array $data 
     * @return boolean
     */
    public function add_admin_group_assoc($data)
    {
        return $this->db->insert('admin_group_assocs', $data);
    }
}
