<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * handler for targeting menu.
 * User: amir
 * Date: 16/03/2016
 * Time: 13:39
 */

class Targeting extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('targetings');
    }

    /* function to handle url traffic percentage
     * user : amir
     * date : 16/03/2016
     */
    public function url_traffic_percentage() {

        $this->data['html'] = $this->load->view('targeting/url_traffic_percentage', '', TRUE);
        $this->load->view('common/container', $this->data);

    }

    public function chart_data() {

        $from   = $this->input->post('sd');
        $to     = $this->input->post('ed');

        $data = $this->targetings->application_data_transfer($from, $to);
        $i = 0;
        foreach($data as $j=>$p)
        {
            if($p[0] > 0 || $p[1] > 0)
            {
                if(isset($this->APP_ID_NAME_PKG[$j]) != null){
                    $datatables[$j] = array (
                        'application'   => $this->APP_ID_NAME_PKG[$j],
                        'transferred'   => round($p[0]/1024,2),
                        'received'      =>  round($p[1]/1024,2)
                    );

                    if($i < 20)
                    {
                        $data_transferred[$j] = array (
                            'application'   => $this->APP_ID_NAME_PKG[$j],
                            'transferred'   => round($p[0]/1024,2),
                            'received'      =>  round($p[1]/1024,2)
                        );
                    }
                    $i++;
                }
            }
        }

        $avg_data = $this->targetings->getAppDataDailyAvg($from, $to);
        $i = 0;
        foreach($avg_data as $j=>$p)
        {
            if($p[0] > 0 || $p[1] > 0)
            {
                if(isset($this->APP_ID_NAME_PKG[$j]) != null) {
                    $dataavg[$j] = array(
                        'application' => $this->APP_ID_NAME_PKG[$j],
                        'transferred' => round($p[0] / 1024, 2),
                        'received' => round($p[1] / 1024, 2)
                    );
                    if($i < 20) {
                        $chartavg[$j] = array(
                            'application' => $this->APP_ID_NAME_PKG[$j],
                            'transferred' => round($p[0] / 1024, 2),
                            'received' => round($p[1] / 1024, 2)
                        );
                    }
                }
                $i++;
            }
        }

        $duration = $this->targetings->getAppTotalDuration($from, $to);
        $i=0;
        foreach($duration as $j=>$p)
        {
            if(isset($this->APP_ID_NAME_PKG[$j]) != null) {
                $durationData[$j] = array(
                    'application' => $this->APP_ID_NAME_PKG[$j],
                    'duration' => $p
                );
                if($i < 20) {
                    $chartDurationData[$j] = array(
                        'application' => $this->APP_ID_NAME_PKG[$j],
                        'duration' => $p
                    );
                }
            }
            $i++;
        }

        $raw_data =   $this->targetings->getTopVisitedUrl($from, $to);
        $i = 1;
        foreach($raw_data as $keys=>$vals)
        {
            if(isset($this->URL_ID_NAME[$keys]) != null){
                $table_url_visited[$keys] = array (
                    'url'   => $this->URL_ID_NAME[$keys],
                    'visited'   => $vals
                );
                if($i < 25)
                {
                    $chart_url_visited[$keys] = array (
                        'url'   => $this->URL_ID_NAME[$keys],
                        'visited'   => $vals
                    );
                }
                $i++;
            }
        }

        $url_traffic['url_traffic'] = $this->targetings->url_traffic_percentages($from, $to);
        $url_traffic['app_traffic'] = $this->targetings->app_traffic_percentages($from, $to);
        $url_traffic['app_data_transfer'] = array_values($data_transferred);
        $url_traffic['app_datatables_transfer'] = array_values($datatables);
        $url_traffic['app_avg'] = array_values($dataavg);
        $url_traffic['chart_avg'] = array_values($chartavg);
        $url_traffic['table_duration'] = array_values($durationData);
        $url_traffic['chart_duration'] = array_values($chartDurationData);
        $url_traffic['table_visited']   = array_values($table_url_visited);
        $url_traffic['chart_visited']   = array_values($chart_url_visited);

        echo json_encode($url_traffic);
    }

    /* function to handle application traffic percentage
     * user : amir
     * date : 16/03/2016
     */
    public function application_traffic_percentage() {

        $this->data['html'] = $this->load->view('targeting/app_traffic_percentage', '', TRUE);
        $this->load->view('common/container', $this->data);

    }

    public function chart_app_data() {

        $app_traffic = $this->targetings->app_traffic_percentages();
        echo json_encode($app_traffic);
    }

    /* function to handle Application data transfered
     * user : amir
     * date : 16/03/2016
     */
    public function application_data_transfered() {

        $this->data['html'] = $this->load->view('targeting/app_data_transfer', '', TRUE);
        $this->load->view('common/container', $this->data);

    }

    /* function to handle url traffic percentage
     * user : amir
     * date : 16/03/2016
     */
    public function daily_average_data_transfered() {

        $avg_data = $this->targetings->getAppDataDailyAvg();
        foreach($avg_data as $j=>$p)
        {
            if($p[0] > 0 || $p[1] > 0)
            {
                if(isset($this->APP_ID_NAME_PKG[$j]) != null) {
                    $dataavg[$j] = array(
                        'application' => $this->APP_ID_NAME_PKG[$j],
                        'transferred' => round($p[0] / 1024, 2),
                        'received' => round($p[1] / 1024, 2)
                    );
                }
            }
        }


        $this->data['html'] = $this->load->view('targeting/daily_average_data_transfer', '', TRUE);
        $this->load->view('common/container', $this->data);

    }

    /* function to handle url traffic percentage
     * user : amir
     * date : 16/03/2016
     */
    public function application_duration() {
        $this->data['html'] = $this->load->view('targeting/app_duration', '', TRUE);
        $this->load->view('common/container', $this->data);
    }


    /* function to handle url traffic percentage
     * user : amir
     * date : 16/03/2016
     */
    public function category_uniq_user() {

    }

    /* function to handle url traffic percentage
     * user : amir
     * date : 16/03/2016
     */
    public function most_visited_url() {
        $this->data['html'] = $this->load->view('targeting/most_visited_url', '', TRUE);
        $this->load->view('common/container', $this->data);
    }

    /* function to handle url traffic percentage
     * user : amir
     * date : 16/03/2016
     */
    public function most_used_application() {

    }

    /* function to handle url traffic percentage
     * user : amir
     * date : 16/03/2016
     */
    public function category_detail_reports() {

    }

}



