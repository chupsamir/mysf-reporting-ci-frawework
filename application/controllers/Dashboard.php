<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	public $data;

	/**
	 * Constructor for Dashboard controller
	 * 
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('dashboards');
	}

	/**
	 * Method index
	 * Used to show dashboard page
	 * 
	 * @return void
	 */
	public function index()
	{

		$this->data['page_title'] = 'mySF Reporting - Home';

		$users = $this->dashboards->dashboardUser();
		$campaignsIDR = $this->dashboards->dashboardCampaign('IDR');
		$campaignsUSD = $this->dashboards->dashboardCampaign('USD');

		$data['total_user'] = $users['hits']['total'];
		$data['uniq_user'] = $users['aggregations']['uniq_user']['value'];
		$data['amk_revenue_idr'] = $campaignsIDR['aggregations']['amk_share']['value'];
		$data['amk_revenue_usd'] = $campaignsUSD['aggregations']['amk_share']['value'];
        $this->data['html'] = $this->load->view('common/dashboard', $data, TRUE);
        $this->load->view('common/container', $this->data);
	}

	public function chart_campaign_data() {

			$this->output->cache(60);
			
			$from	= $this->input->post('sd');
			$to		= $this->input->post('ed');


			$campaignsIDR = $this->dashboards->dashboardCampaign('IDR', $from, $to);
			$campaignsUSD = $this->dashboards->dashboardCampaign('USD', $from, $to);

			$campaigns['amk_revenue_idr']	= $campaignsIDR['aggregations']['amk_share']['value'];
			$campaigns['amk_revenue_usd'] 	= $campaignsUSD['aggregations']['amk_share']['value'];

			$campaigns['sf_revenue_idr']	= $campaignsIDR['aggregations']['sf_share']['value'];
			$campaigns['sf_revenue_usd'] 	= $campaignsUSD['aggregations']['sf_share']['value'];

			$campaigns['chart_traffic'] = 	$this->dashboards->dashboardCampaignCharts('IDR',$from,$to);
			$campaigns['chart_revenue']	=	$this->dashboards->dashboardRevenueCharts('IDR', $from,$to);
			echo json_encode($campaigns);
	}

}