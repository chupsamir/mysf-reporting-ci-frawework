<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller {
    /**
     * Constructor for Menu controller
     * 
     * @return void
     */
    public function __construct() 
    {
        parent::__construct();
    }
    
    /**
     * Method index
     * Used to show `show all menus` page
     * 
     * @return void
     */
    public function index() 
    {
        $this->data['page_title'] = 'mySF Reporting - Show All Menus';
        $data['list'] = $this->menu_model->get_all_menus(0, 1, $this->data['admin_group_id'], $this->input->get('keyword'));
        $this->data['html'] = $this->load->view('menu/index', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method new_menu
     * Used to show new menu page
     * 
     * @param int $parent_id 
     * @return void
     */
    public function new_menu($parent_id = 0) 
    {
        $this->data['page_title'] = 'mySF Reporting - New Menu';
        $menu_types = $this->menu_model->get_all_menu_types();
        $data['menu_types'] = dropdown_list($menu_types, 'menu_type_id', 'menu_type_title');
        $data['parent_id'] = $parent_id;
        $this->data['html'] = $this->load->view('menu/new_menu', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method add_menu
     * Used to add new menu into database
     * 
     * @return void
     */
    public function add_menu() 
    {
        $this->form_validation->set_rules('menu_title', 'menu title', 'required|trim');
        $this->form_validation->set_rules('fk_menu_type_id', 'menu type', 'required');
        $this->form_validation->set_rules('menu_url', 'menu url', 'trim');
        $this->form_validation->set_rules('menu_status', 'menu status', 'required');

        if($this->form_validation->run() == FALSE) 
        {
            $this->new_menu();
        } 
        else 
        {
            $data = array(
                'menu_title' => $this->input->post('menu_title'), 
                'menu_parent_id' => $this->input->post('menu_parent_id'), 
                'fk_menu_type_id' => $this->input->post('fk_menu_type_id'), 
                'menu_url' => $this->input->post('menu_url'), 
                'menu_status' => $this->input->post('menu_status'),
                'menu_icon'     => $this->input->post('menu_icon'),
                'menu_position' => $this->menu_model->get_last_position($this->input->post('menu_parent_id')) + 1
            );

            if($this->menu_model->add($data))
            {
                if($this->input->post('menu_parent_id') != NULL && $this->input->post('menu_parent_id') != 0)
                {
                    redirect('menu/details/' . $this->input->post('menu_parent_id'));
                }
                else
                {
                    redirect('menu'); 
                }
            }
            else
            {
                js_set_message('Fail!', site_url('menu'));
            }
        }
    }
    
    /**
     * Method edit_menu
     * Used to show edit menu page
     * 
     * @param int $id 
     * @param int $parent_id 
     * @return void
     */
    public function edit_menu($id, $parent_id = NULL) 
    {

        if(!$id)
        {
            js_set_message('Data is not exist!', site_url('menu'));
        }
            
        $this->data['page_title'] = 'mySF Reporting - Edit Menu';
        $menu_types = $this->menu_model->get_all_menu_types();
        $data['menu_types'] = dropdown_list($menu_types, 'menu_type_id', 'menu_type_title');
        $data['details'] = $this->menu_model->get_menu($id);
        $this->data['html'] = $this->load->view('menu/edit_menu', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method update_menu
     * Used to update menu details in database
     * 
     * @return void
     */
    public function update_menu() 
    {
        $this->form_validation->set_rules('menu_title', 'menu title', 'required|trim');
        $this->form_validation->set_rules('fk_menu_type_id', 'menu type', 'required');
        $this->form_validation->set_rules('menu_url', 'menu url', 'trim');
        $this->form_validation->set_rules('menu_status', 'menu status', 'required');
        
        if($this->form_validation->run() == FALSE) 
        {
            $this->edit_menu($this->input->post('menu_id'));
        } 
        else 
        {
            $data = array(
                'menu_title' => $this->input->post('menu_title'), 
                'fk_menu_type_id' => $this->input->post('fk_menu_type_id'), 
                'menu_url' => $this->input->post('menu_url'), 
                'menu_status' => $this->input->post('menu_status'),
                'menu_icon'     => $this->input->post('menu_icon')
            );

            if($this->menu_model->update($this->input->post('menu_id'), $data))
            {
                if($this->input->post('menu_parent_id') != NULL && $this->input->post('menu_parent_id') != 0)
                {
                    redirect('menu/details/' . $this->input->post('menu_parent_id'));
                }
                else
                {
                    redirect('menu');
                }
            }
            else
            {
                js_set_message('Fail!', site_url('menu'));
            }
        }
    }
    
    /**
     * Method delete_menu
     * Used to delete menu from database
     * 
     * @param int $id 
     * @param int $parent_id 
     * @return void
     */
    public function delete_menu($id, $parent_id = NULL) 
    {
        if(!$id)
        {
            js_set_message('Data is not exist!', site_url('menu'));
        }
            
        if($this->menu_model->delete($id)) 
        {
            $this->menu_model->delete_admin_group_menu_assoc_by_menu_id($id);
            if($parent_id != NULL && $parent_id != 0)
            {
                redirect('menu/details/' . $parent_id);
            }
            else
            {
                redirect('menu');
            }
        } 
        else 
        {
            js_set_message('Fail!', site_url('menu'));
        }
    }
    
    /**
     * Method details
     * Used to show menu details page
     * 
     * @param int $id 
     * @return void
     */
    public function details($id) 
    {
        if(!$id)
        {
            js_set_message('Data is not exist!', site_url('menu'));
        }
            
        $details = $this->menu_model->get_menu($id);
        if(!$details)
        {
            js_set_message('Data is not exist!', site_url('menu'));
        }
           
        $this->data['page_title'] = 'mySF Reporting - Menu Details';
        $data['details'] = $details;
        $data['submenus'] = $this->menu_model->get_all_menus_by_parent_id($details['menu_id'], '');
        $this->data['html'] = $this->load->view('menu/details', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method move_menu
     * Used to move menu position
     * 
     * @param string $direction 
     * @param int $id 
     * @param int $parent_id 
     * @return void
     */
    public function move_menu($direction, $id, $parent_id = NULL) 
    {
        if(!$direction)
        {
            js_set_message('There is no direction!', site_url('menu'));
        }
            
        if(!$id)
        {
            js_set_message('Data is not exist!', site_url('menu'));
        }
            
        if($this->menu_model->update_position($direction, $id)) 
        {
            if($parent_id != NULL && $parent_id != 0)
            {
                redirect('menu/details/' . $parent_id);
            }
            else
            {
                redirect(site_url('menu'));
            }
        } 
        else 
        {
            js_set_message('Fail!', site_url('menu'));
        }
    }
}