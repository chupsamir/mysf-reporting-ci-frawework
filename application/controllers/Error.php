<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends MY_Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->output->set_status_header('404');
        $data['heading'] = 'Ooops! We\'ve got a problem!';
        $data['message']= '';
        $this->load->view('error/error', $data);
    }
}