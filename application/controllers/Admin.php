<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {
    /**
     * Constructor for Admin controller
     * 
     * @return void
     */
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('admin_model', 'admin_model');
        $this->load->helper('dump_helper');
    }
    
    /**
     * Method index
     * Used to show `show all admins` page
     * 
     * @return void
     */
    public function index() 
    {
        $this->data['page_title'] = 'mySF Reporting - Show All Admins';

        if($this->data['admin_group_id'] == 1)
        {
            $data['list'] = $this->admin_model->get_all_admins();
        }
        else
        {
            $data['list'] = $this->admin_model->get_all_non_root_admins();
        }
            
        $this->data['html'] = $this->load->view('admin/index', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method group
     * Used to show manage admin group page
     * 
     * @return void
     */
    public function group() 
    {
        $this->data['page_title'] = 'mySF Reporting - Manage Admin Group';
        
        if($this->data['admin_group_id'] == 1) 
        {
            $data['list'] = $this->admin_model->get_all_groups();
            $data['menus'] = $this->menu_model->get_all_menus_by_parent_id(0);
        } 
        else 
        {
            $data['list'] = $this->admin_model->get_all_non_root_groups();
            $data['menus'] = $this->menu_model->get_all_non_root_menus_by_parent_id(0);
        }
        
        $this->data['html'] = $this->load->view('admin/group', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method add_group
     * Used to add new group into database
     * 
     * @return void
     */
    public function add_group() 
    {
        $this->form_validation->set_rules('admin_group_name', 'group name', 'required|trim');
        $this->form_validation->set_rules('fk_menu_id[]', 'privileges', 'required');

        if($this->form_validation->run() == FALSE) 
        {
            $this->group();
        } 
        else 
        {
            $data = array(
                'admin_group_name' => $this->input->post('admin_group_name')
            );

            $admin_group_id = $this->admin_model->add_group($data);

            if($admin_group_id) 
            {
                foreach($this->input->post('fk_menu_id') as $menu) 
                {
                    $assoc['fk_admin_group_id'] = $admin_group_id;
                    $assoc['fk_menu_id'] = $menu;
                    $this->menu_model->add_admin_group_menu_assoc($assoc);
                }
                
                redirect(site_url('admin/group'));
            } 
            else 
            {
                js_set_message('Fail!', site_url('admin/group'));
            }
        }
    }
    
    /**
     * Method edit_group
     * Used to show edit group page
     * 
     * @param int $id 
     * @return void
     */
    public function edit_group($id) 
    {
		
        if(!$id)
        {
            js_set_message('Data is not exist!', site_url('admin/group'));
        }
        
        $this->data['page_title'] = 'mySF Reporting - Edit Admin Group';
        
        if($this->data['admin_group_id'] == 1) 
        {
            $data['list'] = $this->admin_model->get_all_groups();
            $data['menus'] = $this->menu_model->get_all_menus_by_parent_id(0);
        } 
        else 
        {
            $data['list'] = $this->admin_model->get_all_non_root_groups();
            $data['menus'] = $this->menu_model->get_all_non_root_menus_by_parent_id(0);
        }
        
        $data['details'] = $this->admin_model->get_group($id);
        $this->data['html'] = $this->load->view('admin/edit_group', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method update_group
     * Used to update group details in database
     * 
     * @return void
     */
    public function update_group() 
    {
        $this->form_validation->set_rules('admin_group_name', 'group name', 'required|trim');
        $this->form_validation->set_rules('fk_menu_id[]', 'privileges', 'required');

        if($this->form_validation->run() == FALSE) 
        {
            $this->edit_group($this->input->post('admin_group_id'));
        } 
        else 
        {
            $data = array(
                'admin_group_name' => $this->input->post('admin_group_name')
            );

            if($this->admin_model->update_group($this->input->post('admin_group_id'), $data)) 
            {
                $this->menu_model->delete_admin_group_menu_assoc_by_group_id($this->input->post('admin_group_id'));
                
                foreach($this->input->post('fk_menu_id') as $menu) 
                {
                    $assoc['fk_admin_group_id'] = $this->input->post('admin_group_id');
                    $assoc['fk_menu_id'] = $menu;
                    $this->menu_model->add_admin_group_menu_assoc($assoc);
                }
                
                redirect(site_url('admin/group'));
            } 
            else 
            {
                js_set_message('Fail!', site_url('admin/group'));
            }
        }
    }
    
    /**
     * Method delete_group
     * Used to delete group from database
     * 
     * @param int $id 
     * @return void
     */
    public function delete_group($id) 
    {
        if(!$id)
        {
            js_set_message('Data is not exist!', site_url('admin/group'));
        }
        
        if($this->admin_model->delete_group($id)) 
        {
            $this->menu_model->delete_admin_group_menu_assoc_by_group_id($id);
            $this->admin_model->delete_admin_group_assoc_by_group_id($id);
            redirect(site_url('admin/group'));
        } 
        else 
        {
            js_set_message('Fail!', site_url('admin/group'));
        }
    }
    
    /**
     * Method new_admin
     * Used to show new admin page
     * 
     * @return void
     */
    public function new_admin() 
    {
        $this->data['page_title'] = 'mySF Reporting - New Admin';

        if($this->data['admin_group_id'] == 1)
        {
            $data['groups'] = $this->admin_model->get_all_groups();
        }
        else
        {
            $data['groups'] = $this->admin_model->get_all_non_root_groups();
        }
            
        $this->data['html'] = $this->load->view('admin/new_admin', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method add_admin
     * Used to add new admin into database
     * 
     * @return void
     */
    public function add_admin() 
    {
        $this->form_validation->set_rules('admin_uname', 'username', 'required|trim');
        $this->form_validation->set_rules('admin_pwd', 'password', 'required|trim');
        $this->form_validation->set_rules('group_id[]', 'groups', 'required');
        
        if($this->form_validation->run() == FALSE) 
        {
            $this->new_admin();
        } 
        else 
        {
            $data = array(
                'admin_uname' => $this->input->post('admin_uname'), 
                'admin_pwd' => sha1($this->config->item('encryption_key') . $this->input->post('admin_pwd')), 
                'admin_status' => $this->input->post('admin_status')
            );
            
            $admin_id = $this->admin_model->add($data);

            if($admin_id) 
            {
                foreach($this->input->post('group_id') as $data) 
                {
                    $assoc['fk_admin_group_id'] = $data;
                    $assoc['fk_admin_id'] = $admin_id;
                    $this->admin_model->add_admin_group_assoc($assoc);
                }

                redirect('admin');
            } 
            else 
            {
                js_set_message('Fail!', site_url('admin'));
            }
        }
    }
    
    /**
     * Method edit_admin
     * Used to show edit admin page
     * 
     * @param int $id 
     * @return void
     */
    public function edit_admin($id) 
    {
        if(!$id)
        {
            js_set_message('Data is not exist!', site_url('admin'));
        }
            
        $this->data['page_title'] = 'mySF Reporting - Edit Admin';

        if($this->data['admin_group_id'] == 1)
        {
            $data['groups'] = $this->admin_model->get_all_groups();
        }
        else
        {
            $data['groups'] = $this->admin_model->get_all_non_root_groups();
        }
            
        $data['details'] = $this->admin_model->get_admin($id);
        $this->data['html'] = $this->load->view('admin/edit_admin', $data, TRUE);
        $this->load->view('common/container', $this->data);
    }
    
    /**
     * Method update_admin
     * Used to update admin details in database
     * 
     * @return void
     */
    public function update_admin() 
    {
        $this->form_validation->set_rules('admin_pwd', 'password', 'trim');
        $this->form_validation->set_rules('group_id[]', 'groups', 'required');
        
        if($this->form_validation->run() == FALSE) 
        {
            $this->edit_admin($this->input->post('admin_id'));
        } 
        else 
        {
            if($this->input->post('admin_pwd') != NULL) 
            {
                $pwd = sha1($this->config->item('encryption_key') . $this->input->post('admin_pwd'));
            } 
            else 
            {
                $details = $this->admin_model->get_admin($this->input->post('admin_id'));
				$pwd = $details['admin_pwd'];
            }
            
            $data = array(
                'admin_uname' => $this->input->post('admin_uname'), 
                'admin_pwd' => $pwd, 
                'admin_status' => $this->input->post('admin_status')
            );

            if($this->admin_model->update($this->input->post('admin_id'), $data)) 
            {
                $this->admin_model->delete_admin_group_assoc_by_admin_id($this->input->post('admin_id'));

                foreach($this->input->post('group_id') as $data) 
                {
                    $assoc['fk_admin_group_id'] = $data;
                    $assoc['fk_admin_id'] = $this->input->post('admin_id');
                    $this->admin_model->add_admin_group_assoc($assoc);
                }

                redirect('admin');
            } 
            else 
            {
                js_set_message('Fail!', site_url('admin'));
            }
        }
    }
    
    /**
     * Method delete_admin
     * Used to delete admin from database
     * 
     * @param int $id 
     * @return void
     */
    public function delete_admin($id) 
    {
        if(!$id)
        {
            js_set_message('Data is not exist!', site_url('admin'));
        }
        
        if($this->admin_model->delete($id)) 
        {
            $this->admin_model->delete_admin_group_assoc_by_admin_id($id);
            redirect('admin');
        } 
        else 
        {
            js_set_message('Fail!', site_url('admin'));
        }
    }
}