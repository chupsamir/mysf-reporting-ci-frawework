<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class to handle all financial function
 * User: amir
 * Date: 16/03/2016
 * Time: 13:52
 */

class Financial extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('finance');

    }

    /* function to handle All campaign List
     * user : amir
     * date : 16/03/2016
     */
    public function all_campaign_list() {

        $this->data['html'] = $this->load->view('financial/all_campaign_list', '', TRUE);
        $this->load->view('common/container', $this->data);

    }

    /* function to handle All campaign List
     * user : amir
     * date : 16/03/2016
     */
    public function schedule_listing() {
        echo 'schedule_listing';

    }

    /* function to handle All campaign List
     * user : amir
     * date : 16/03/2016
     */
    public function daily_reporting_idr() {

        $this->data['html'] = $this->load->view('financial/daily_reporting_idr', '', TRUE);
        $this->load->view('common/container', $this->data);

    }

    public function daily_reporting_usd() {

        $this->data['html'] = $this->load->view('financial/daily_reporting_usd', '', TRUE);
        $this->load->view('common/container', $this->data);

    }

    public function chart_daily_usd() {

        $from	= $this->input->post('sd');
        $to		= $this->input->post('ed');
        $price_type =   $this->input->post('pt');
        $currency = $this->input->post('curr');
        $content_type = $this->input->post('ct');
        $client_id  =   $this->input->post('clid');


        if (isset($currency)) {
            $monthly = $this->finance->getMonthlyReport($currency, $from, $to, $price_type,$content_type,$client_id);
            $details = $this->finance->getMonthlyDetailsReport($currency, $from, $to, $price_type,$content_type,$client_id);

            foreach ($details as $key => $val) {
                foreach ($val as $value) {
                    $summary_type[$key]['type'] = $key;
                    $summary_type[$key]['price'] = $value['price'];
                    $summary_type[$key]['total_price'] = array_sum(array_column($val, 'price'));
                    $summary_type[$key]['click'] = array_sum(array_column($val, 'click'));
                    $summary_type[$key]['budget'] = array_sum(array_column($val, 'budget'));
                    $summary_type[$key]['sf'] = array_sum(array_column($val, 'SF_SHARE'));
                    $summary_type[$key]['monthly_revenue'] = array_sum(array_column($val, 'click')) * $value['price'];
                    $summary_type[$key]['sf_share'] = array_sum(array_column($val, 'click')) * $value['price'] * $value['SF_SHARE'] / 100;
                    $summary_type[$key]['amk_share'] = array_sum(array_column($val, 'click')) * $value['price'] * $value['AMK_SHARE'] / 100;
                }
            }

            foreach ($details as $key => $val) {
                for ($i=0; $i<count($val); $i++) {
                    $details_type[$key][$i] = array(
                        'name'  => $details[$key][$i]['name'],
                        'date'  => $details[$key][$i]['date'],
                        'price' => $details[$key][$i]['price'],
                        'price_type' => $details[$key][$i]['price_type'],
                        'click'      => $details[$key][$i]['click'],
                        'budget'     => $details[$key][$i]['budget'],
                        'scaps'      => $details[$key][$i]['scaps'],
                        'status'     => round($details[$key][$i]['price']* $details[$key][$i]['click'] * 100/$details[$key][$i]['budget'],2),     //$details[$key][$i]['status'],
                        'client'     => $details[$key][$i]['client'],
                        'sf_share'          => $details[$key][$i]['SF_SHARE'],
                        'amk_share'         => $details[$key][$i]['AMK_SHARE'],
                        'revenue_total'     => $details[$key][$i]['click'] * $details[$key][$i]['price'],
                        'sf_revenue_share'  => $details[$key][$i]['click'] * $details[$key][$i]['price'] * $details[$key][$i]['SF_SHARE']/100,
                        'amk_revenue_share' => $details[$key][$i]['click'] * $details[$key][$i]['price'] * $details[$key][$i]['AMK_SHARE']/100
                    );
                }
            }

            $summary = array_values($summary_type);

            $daily[0] = NULL;
            $daily[1] = NULL;

        } else {
            $daily[0] = $this->finance->get_revenue_daily('USD', $from, $to, $price_type);
            $daily[1] = $this->finance->get_revenue_daily('IDR', $from, $to, $price_type);
            $campaign_list = array_values($this->finance->campaignDataTable('',$from, $to));

            for ($j = 0; $j < 2; $j++) {
                for ($i = 0; $i < count($daily[$j]); $i++) {


                    if ($daily[$j][$i]['received'] != 0 && $daily[$j][$i]['served'] != 0) {
                        $daily[$j][$i]['receive_percentage'] = round($daily[$j][$i]['received'] * 100 / $daily[$j][$i]['served'], 2);
                    } else {
                        $daily[$j][$i]['receive_percentage'] = 0;
                    }

                    if ($daily[$j][$i]['impression'] != 0 && $daily[$j][$i]['served']) {
                        $daily[$j][$i]['display_percentage'] = round($daily[$j][$i]['impression'] * 100 / $daily[$j][$i]['received'], 2);
                    } else {
                        $daily[$j][$i]['display_percentage'] = 0;
                    }

                    if ($daily[$j][$i]['clicks'] != 0 && $daily[$j][$i]['impression']) {
                        $daily[$j][$i]['ctr'] = round($daily[$j][$i]['clicks'] * 100 / $daily[$j][$i]['impression'], 2);
                    } else {
                        $daily[$j][$i]['ctr'] = 0;
                    }

                }
            }

            $monthly = NULL;
            $summary = NULL;
        }

        $final['usd'] = $daily[0];
        $final['idr'] = $daily[1];
        $final['monthly'] = (!isset($monthly)) ? NULL: $monthly;
        $final['summary'] = (!isset($summary)) ? NULL: $summary;
        $final['details'] = (!isset($details)) ? NULL: $details;
        $final['text'] = (!isset($details_type['Text'])) ? NULL: $details_type['Text'];
        $final['apk'] = (!isset($details_type['APK'])) ? NULL: $details_type['APK'];
        $final['image'] = (!isset($details_type['Image'])) ? NULL: $details_type['Image'];
        $final['audio'] = (!isset($details_type['Audio'])) ? NULL: $details_type['Audio'];
        $final['video'] = (!isset($details_type['Video'])) ? NULL: $details_type['Video'];
        $final['shortcut'] = (!isset($details_type['Shortcut Icon'])) ? NULL: $details_type['Shortcut Icon'];
        $final['campaign']  = (!isset($campaign_list)) ? NULL: $campaign_list;

        echo json_encode($final);
    }

    /* function to handle All campaign List
     * user : amir
     * date : 16/03/2016
     */
    public function monthly_report() {

        $this->data['html'] = $this->load->view('financial/monthly_reporting', '', TRUE);
        $this->load->view('common/container', $this->data);


    }

    public function data_monthly () {


        $data = $this->finance->getMonthlyReport();

    }
}