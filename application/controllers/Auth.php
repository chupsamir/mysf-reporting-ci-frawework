<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	public $data;

    /**
     * Constructor for Auth controller
     * Note that this controller is child controller of CI_Controller, not MY_Controller like others
     * 
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model', 'auth_model');
        $this->load->model('menu_model', 'menu_model');
        
        if($this->auth_model->is_logged()) 
        {
            $session_data = $this->session->userdata('mc_admin_session');
            $this->data['admin_id'] = $session_data['admin_id'];
            $this->data['admin_uname'] = $session_data['admin_uname'];
            $this->data['admin_group_id'] = $session_data['admin_group_id'];
            $this->form_validation->set_error_delimiters('', '');
            $this->load->helper('common');
            
            $menu_data['current_class'] = $this->router->fetch_class();
            $menu_data['current_method'] = $this->router->fetch_method();
            $menu_data['sidebar_menu_list'] = $this->menu_model->get_all_menus(0, 1, $this->data['admin_group_id']);
            $this->data['menu_list'] = $this->load->view('common/menu_list', $menu_data, TRUE);
        }
	}

    /**
     * Method index
     * Used to show dashboard page
     * If user is not logged in, will show login page
     * 
     * @return void
     */
	public function index()
	{
		if($this->auth_model->is_logged()) 
		{
            redirect(site_url('dashboard'));
        } 
        else 
        {
            $this->data['page_title'] = 'Welcome to mySF Reporting';
			$this->load->view('auth/index', $this->data);
        }
	}

    /**
     * Method login
     * Used to authenticate admin login
     * If login success will be redirected to dashboard page
     * 
     * @return void
     */
	public function login()
	{
        //die('login function');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if($this->form_validation->run() == FALSE) 
        {
            $this->session->set_flashdata('error_msg', validation_errors());
            redirect(site_url('auth/'));
        } 
        else 
        {
            $_POST['password'] = sha1($this->config->item('encryption_key') . $this->input->post('password'));
            
            if($this->auth_model->authenticate_admin($this->input->post())) 
            {
                redirect(site_url('auth/'));
            } 
            else 
            {
                $this->session->set_flashdata('error_msg', 'Warning: username/password is not valid!');
                redirect(site_url('auth/'));
            }
        }
	}

    /**
     * Method logout
     * Used to sign out from CMS
     * 
     * @return void
     */
	public function logout()
	{
		$this->session->sess_destroy();
        redirect(site_url());
	}
}