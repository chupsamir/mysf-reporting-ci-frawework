<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['socket_type'] = 'tcp'; //`tcp` or `unix`
$config['socket'] = '/var/run/redis.sock'; // in case of `unix` socket type
$config['host'] = '202.70.52.14';
$config['password'] = NULL;
$config['port'] = 5006;
$config['timeout'] = 0;