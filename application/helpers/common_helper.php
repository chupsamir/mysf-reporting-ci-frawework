<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * js_set_message
 * set javascript window alert and redirect the page
 **/
if(!function_exists('js_set_message')) 
{
    function js_set_message($msg = '', $url = '') 
    {
        echo '<script>javascript:alert(\'' . $msg . '\'); window.location.replace(\'' . $url . '\')</script>';
        exit();
    }
}

/**
 * dropdown_list
 * convert multidimensional array into simplified single array
 * mostly used to create array list for dropdown form
 **/
if(!function_exists('dropdown_list')) 
{
    function dropdown_list($array, $key, $value, $default = null) 
    {
        $result = array();
        
        if(isset($default) && $default != null)
        {
            $result[''] = $default;
        }
        
        for($i=0;$i<count($array);$i++) 
        {
            $result[$array[$i][$key]] = $array[$i][$value];
        }
        
        return $result;
    }
}

/**
 * create_dir
 * create directory if not existed
 **/
if(!function_exists('create_dir')) 
{
    function create_dir($path) 
    {
        if(!file_exists($path)) 
        {
            @mkdir($path);
            @chmod($path, 0777);
            @copy(DATAPATH . 'index.html', $path . '/index.html');
            clearstatcache();
        }
    }
}

/**
 * array_sort_by_column
 * sorting array by column
 **/
if(!function_exists('array_sort_by_column')) 
{
    function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) 
    {
        $sort_col = array();
        foreach ($arr as $key=> $row) 
        {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }
}