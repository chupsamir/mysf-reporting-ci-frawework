<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finance {

    //initial variable
    private $_CI;
    private $_ES;

    //construct 
    public function __construct() {
        $this->_CI =& get_instance();
        $this->_CI->load->helper('url');

        $this->_HOST    = $this->_CI->config->item('ES_HOST');

         $this->_ES = Elasticsearch\ClientBuilder::create()
            ->setHosts($this->_HOST)
            ->setRetries(0)
            ->build();
        
          }

    /* daily reports function
        by amir@20160311
    */

    public function get_revenue_daily($currency, $from=NULL, $to=NULL, $price_type=NULL, $client_id=NULL) {

        $index = "mysf_campaigndata";
        $type = "cam";
        $params = array();
        $params['index'] = $index;
        $params['type']   = $type;
        $string = array();
        $camptype="0";
        //$size = totalDocument('mysf_campaigndata');

        $string[] = '{ "match": {  "currency": "'.$currency.'" } }';

        if($camptype !=null)
            $string[] ='{ "match": {  "camptype": "'.$camptype.'" } }';

        if ($price_type) 
            $string[] ='{ "match": {  "price_type": "'.$price_type.'" } }';

        if ($client_id)
            $string[] = '{  "terms":{ "client_id":['.$client_id.'] } }';

        if(!empty($from) && !empty($to))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$from.'", "lte":"'.$to.'" } } } ';

        $final_str='';

        if(!empty($fromDate) && !empty($toDate))
        $string[] = '{ "range": { "DD.ST": { "gte":"'.$from.'", "lte":"'.$to.'" } } } ';

        $final_str='';
        if(count($string) > 0)
                {
                    $final_str = '"query":{"bool":{"must":['.implode(',',$string).']}';
                }

          $json1 = '{"size": 0,
            '.$final_str.'
           },

           "aggs": {
                    "monthly": {
                       "date_histogram": {
                          "field": "DD.ST",
                          "min_doc_count":0,
                          "interval": "day",
                          "format": "yyyy-MM-dd",
                          "min_doc_count" : 0, 
                          "extended_bounds" : { 
                              "min" : "'.$from.'",
                              "max" : "'.$to.'"
                            }
                       },
                       "aggs": {
                          "priceXclicks": {
                             "sum": {
                                "script":"_source.price * _source.clicks"
                             }
                          },
                           "send": {
                             "sum": {
                              "field": "events.SEND"
                             }
                          },
                          "received": {
                             "sum": {
                              "field": "events.RECEIVED"
                             }
                          },
                          "viewed": {
                             "sum": {
                              "field": "events.VIEWED"
                             }
                          },
                           "price": {
                             "sum": {
                              "field": "price"
                             }
                          },
                           "clicks": {
                             "sum": {
                              "field": "clicks"
                             }
                          },
                           "sf_share": {
                             "sum": {
                               "script": "_source.price * _source.clicks * (_source.DD.TAG.SF_SHARE.value) * 0.01"
                             }
                          },
                           "amk_share": {
                             "sum": {
                               "script": "_source.price * _source.clicks * (_source.DD.TAG.AMK_SHARE) * 0.01"
                             }
                          }
                       }
                    }
                 }
           }';

        $finalResult='';
        $params['body'] = $json1;
        
        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
        
        return $stCode;
        
        }

        $cnt = count($results['aggregations']['monthly']['buckets']);
        $final ='';
        
        if($cnt>0){
            $finalResult = $results['aggregations']['monthly']['buckets'];

            for ($i=0; $i < count($finalResult); $i++ ) {

                $final[$i] = array(
                    "date"          => date("Y-m-d", strtotime($finalResult[$i]['key_as_string'])),
                    "inventory"     =>$finalResult[$i]['send']['value'],
                    "served"        =>$finalResult[$i]['send']['value'],
                    "received"      =>$finalResult[$i]['received']['value'],
                    "impression"    =>$finalResult[$i]['viewed']['value'],
                    "clicks"        =>$finalResult[$i]['clicks']['value'],
                    "Price"         =>$finalResult[$i]['price']['value'],
                    "revenue"       =>$finalResult[$i]['priceXclicks']['value'],
                    "sf_share"      =>$finalResult[$i]['sf_share']['value'],
                    "amk_share"     =>$finalResult[$i]['amk_share']['value']
                );
            }
        }

        return $final;
    }

    /* daily reports function
        by amir@20160311
    */

    function campaignDataTable($camptype=NULL,$fromDate=NULL,$toDate=NULL){
        $params = array();
        $params['index']    = 'mysf_campaigndata';
        $params['type']     = 'cam';

        $string = array();
        if(!empty($fromDate) && !empty($toDate))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$fromDate.'", "lte":"'.$toDate.'" } } } ';
        if($camptype !='')
            $string[] = '{ "terms":{ "camptype":['.$camptype.'] } }';
        $final_str='';
        if(count($string) > 0)
        {
            $final_str = '"query":{"bool":{"must":['.implode(',',$string).']} } ,';
        }

        $size = $this->totalDocument('mysf_campaigndata');
        $orderfield ='DD.ID';
        $orderby ='desc';
        $from='0';

        $json1 = '
            {
            '.$final_str.'
               "_source": [
                  "DD.NM","DD.ST","DD.ET","DD.CD","sty","campid"
               ],
               "sort": { "'.$orderfield.'": { "order": "'.$orderby.'" }},
                "size":"'.$size.'","from":"'.$from.'"
            }';

        $finalResult='';
        $params['body'] = $json1;
        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
        $dataResult = $results['hits']['hits'];
        $content_typ_ar = array("3"=>"Audio","5"=>"APK","7"=>"Video","9"=>'Image',"10"=>"Text","14"=>"Shortcut Icon");

        foreach ($dataResult as $key => $value) {
            foreach ($content_typ_ar as $data => $type) {
                if ($value['_source']['sty'] == $data) {

                    $finalResult[$key] = array(
                        'name' => $value['_source']['DD']['NM'],
                        'created'   => date("Y-m-d", strtotime($value['_source']['DD']['CD'])),
                        'start' => date("Y-m-d", strtotime($value['_source']['DD']['ST'])),
                        'end' => date("Y-m-d", strtotime($value['_source']['DD']['ET'])),
                        'type' => $type
                    );
                }
            }
        }

        return $finalResult;
    }


    function getClienttName(){
         $pdo = new PDO('mysql:host='.CMS_HOST.';dbname='.CMS_DATABASE.';charset=utf8', CMS_DBUSER,CMS_DBPASS);
         $sqlquery = "SELECT id,cname FROM `tbl_client`";
         $stmt = $pdo->prepare( $sqlquery);
         $stmt->execute();
         $contentList = $stmt->fetchAll();
         $stmt->closeCursor();
         $pdo = null;
         foreach($contentList as $fr){
          $final[$fr['id']] = $fr['cname'];
    }

    return $final;

    }

    public function totalDocument($index=NULL)
    {
        $index = (!empty($indexp)) ? $indexp : $index;
        $totCnt = $this->_ES->count(array('index' => $index));
        return $totCnt['count'];
    }

    /*get monthly report start
    * used to view chart monthly report
    */
    function getMonthlyReport($currency, $fromDate=NULL,$toDate=NULL,$price_type=NULL,$content_type=NULL,$client=NULL){

        //$currency = 'IDR';
        $params = array();
        $params['index'] = 'mysf_campaigndata';
        $params['type'] = 'cam';
        $string = array();
//For content type image, text, video
        if($content_type)
            $string[] = '{ "terms":{ "sty":['.$content_type.'] } }';
//For Client
        if($client)
            $string[] = '{  "terms":{ "client_id":['.$client.'] } }';
//for price type CPM, CPI, CPC
        if($price_type)
//$string[] = '{        "query": { "match": { "price_type":"'.$price_type.'" } } } ';
            $string[] ='{ "match": {  "price_type": "'.$price_type.'" } }';

        if($currency)
            $string[] ='{ "match": {  "currency": "'.$currency.'" } }';


        if(!empty($fromDate) && !empty($toDate))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$fromDate.'", "lte":"'.$toDate.'" } } } ';

        $final_str='';
        if(count($string) > 0)
        {
            $final_str = '"query":{"bool":{"must":['.implode(',',$string).']}';
        }

        $json1 = '{"size": 0,
    '.$final_str.'
   },

         "aggs": {
            "monthly": {
               "date_histogram": {
                  "field": "DD.ST",
                  "min_doc_count": 0,
                  "interval": "day",
                  "format": "yyyy-MM-dd"
               },
               "aggs": {
                  "sum": {
                     "sum": {
                        "script":"doc[\'price\'].value * doc[\'clicks\'].value"
                     }
                  }
               }
            }
         } }';

        $final='';
        $params['body'] = $json1;
        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }

        unset($results['aggregations']['final_bucket']['doc_count']);
        $cnt = count($results['aggregations']['monthly']['buckets']);
        if($cnt>0){
            $finalResult = $results['aggregations']['monthly']['buckets'];
            for ($i=0; $i < count($finalResult); $i++ ) {

                $final[$i] = array (
                    'date' => date("Y-m-d", strtotime($finalResult[$i]['key_as_string'])),
                    'value' => $finalResult[$i]['sum']['value']
                );

            }
        }

        return $final;
    }

    function getMonthlyDetailsReport($currency, $fromDate=NULL,$toDate=NULL,$price_type=NULL,$content_type=NULL,$client=NULL){

        //$currency = 'USD';
      //  $currency = '';
        $params = array();
        $params['index'] = 'mysf_campaigndata';
        $params['type'] = 'cam';
        $string = array();
//For content type image, text, video
        if($content_type)
            $string[] = '{ "terms":{ "sty":['.$content_type.'] } }';
//For Client
        if($client)
            $string[] = '{  "terms":{ "client_id":['.$client.'] } }';
//for price type CPM, CPI, CPC
        if($price_type)
            $string[] ='{ "match": {  "price_type": "'.$price_type.'" } }';
// For currency
        if($currency)
            $string[] ='{ "match": {  "currency": "'.$currency.'" } }';

        if(!empty($fromDate) && !empty($toDate))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$fromDate.'", "lte":"'.$toDate.'" } } } ';

        $final_str='';

        if(count($string) > 0)
        {
            $final_str = '"query":{"bool":{"must":['.implode(',',$string).']}';
        }

        $json1 = '{
    '.$final_str.'
   },

      "_source": [
      "type",
      "DD.NM","DD.ST",
      "price_type",
      "price",
      "clicks",
      "daily_budget",
      "scap",
      "status",
      "currency",
          "client_id",
          "DD.TAG.SF_SHARE","DD.TAG.AMK_SHARE",
          "sty"
   ],
   "sort": [
      {
         "clicks": {
            "order": "desc"
         }
      }
   ]

                 }';

        $finalResult='';
        $params['body'] = $json1;

        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
//$cnt = count($results['aggregations']['monthly']['doc_count']);

        $content_typ_ar = array("3"=>"Audio","5"=>"APK","7"=>"Video","9"=>'Image',"10"=>"Text","14"=>"Shortcut Icon");
        $finalResult = $results['hits']['hits'];

        foreach($finalResult as $fr){

            $fr['_source']['DD']['ST']  = date("Y-m-d", strtotime($fr['_source']['DD']['ST']));

            foreach ($content_typ_ar as $key => $value) {

                if($fr['_source']['sty']==$key)
                {
                    $fdata[$value][]   = array(
                        'name' =>$fr['_source']['DD']['NM'],
                        'date' =>$fr['_source']['DD']['ST'],
                        'price_type'=>$fr['_source']['price_type'],
                        'price'=>$fr['_source']['price'],
                        'click'=>$fr['_source']['clicks'],
                        'budget'=>$fr['_source']['daily_budget'],
                        'scaps'=>$fr['_source']['scap'],
                        'status'=>$fr['_source']['status'],
                        'client'=>$fr['_source']['client_id'],
                        "SF_SHARE"=>$fr['_source']['DD']['TAG']['SF_SHARE'],
                        "AMK_SHARE"=>$fr['_source']['DD']['TAG']['AMK_SHARE']
                    );

                }

            }

        }
        return $fdata;
    }

}