<?php
/**
 * Library for Targeting Menu
 * User: amir
 * Date: 17/03/2016
 * Time: 22:29
 */

class Targetings
{

    //initial variable
    private $_CI;
    private $_ES;

    //construct
    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->_CI->load->helper('url');

        $this->_HOST = $this->_CI->config->item('ES_HOST');

        $this->_ES = Elasticsearch\ClientBuilder::create()
            ->setHosts($this->_HOST)
            ->setRetries(0)
            ->build();

    }

    function url_traffic_percentages($fromDate=NULL,$toDate=NULL){

        $params = array();
        $params['index']  = 'mysf_urldatalog';
        $params['type']   = 'url';

        if(!empty($fromDate) && !empty($toDate)) {
            $json1 = '
	{
   "size": 0,
   "aggs": {
      "final_bucket": {
         "filter": {
            "range": {
               "RU": {
                   "from": "'.$fromDate.'",
                    "to": "'.$toDate.'"
               }
            }
         },
         "aggs": {
            "0": {
               "sum": {
                  "field": "A1"
               }
            },
            "1": {
               "sum": {
                  "field": "A2"
               }
            },
            "2": {
               "sum": {
                  "field": "A3"
               }
            },
            "3": {
               "sum": {
                  "field": "A4"
               }
            },
            "4": {
               "sum": {
                  "field": "A5"
               }
            },
            "5": {
               "sum": {
                  "field": "A6"
               }
            },
            "6": {
               "sum": {
                  "field": "A7"
               }
            },
            "7": {
               "sum": {
                  "field": "A8"
               }
            }
         }
      }
   }

}';
        }
        else
        {
            $json1 = '
	{
   "size": 0,
   "aggs": {
      "0": {
         "sum": {
            "field": "A1"
         }
      },
      "1": {
         "sum": {
            "field": "A2"
         }
      },
      "2": {
         "sum": {
            "field": "A3"
         }
      },
      "3": {
         "sum": {
            "field": "A4"
         }
      },
      "4": {
         "sum": {
            "field": "A5"
         }
      },
      "5": {
         "sum": {
            "field": "A6"
         }
      },
      "6": {
         "sum": {
            "field": "A7"
         }
      },
      "7": {
         "sum": {
            "field": "A8"
         }
      }
   }

}';
        }
        $params['body'] = $json1;

        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
        if(!empty($fromDate) && !empty($toDate)) {
            unset($results['aggregations']['final_bucket']['doc_count']);
            $ar = $results['aggregations']['final_bucket'];

            $interval   = array (
                '0-3',
                '3-6',
                '3-6',
                '6-9',
                '9-12',
                '12-15',
                '15-18',
                '18-21',
                '21-24'
            );
            for ($i=0; $i<count($ar); $i++) {

                $final[$i]  =  array(
                    'interval'  =>  $interval[$i],
                    'data'  => $ar[$i]['value']

                );
            }

        } else {
            $interval   = array (
                '0-3',
                '3-6',
                '3-6',
                '6-9',
                '9-12',
                '12-15',
                '15-18',
                '18-21',
                '21-24'
            );
            $ar = $results['aggregations'];
            for ($i=0; $i<count($ar); $i++) {
                $final[$i]  =  array(
                    'interval'  =>  $interval[$i],
                    'data'  => $ar[$i]['value']

                );
            }

        }
        $cnt = count($ar);
        if($cnt<=0) return 0;

        return $final;
    }

    function app_traffic_percentages($fromDate=NULL,$toDate=NULL){

        $params = array();
        $params['index']  = 'mysf_appgenlog';
        $params['type']   = 'app1';

        if(!empty($fromDate) && !empty($toDate)) {
            $json1 = '
                {
               "size": 0,
               "aggs": {
                  "final_bucket": {
                     "filter": {
                        "range": {
                           "RU": {
                               "from": "'.$fromDate.'",
                                "to": "'.$toDate.'"
                           }
                        }
                     },
                     "aggs": {
                        "0": {
                           "sum": {
                              "field": "A1"
                           }
                        },
                        "1": {
                           "sum": {
                              "field": "A2"
                           }
                        },
                        "2": {
                           "sum": {
                              "field": "A3"
                           }
                        },
                        "3": {
                           "sum": {
                              "field": "A4"
                           }
                        },
                        "4": {
                           "sum": {
                              "field": "A5"
                           }
                        },
                        "5": {
                           "sum": {
                              "field": "A6"
                           }
                        },
                        "6": {
                           "sum": {
                              "field": "A7"
                           }
                        },
                        "7": {
                           "sum": {
                              "field": "A8"
                           }
                        }
                     }
                  }
               }

            }';
        }
        else
        {
            $json1 = '
                {
               "size": 0,
               "aggs": {
                  "0": {
                     "sum": {
                        "field": "A1"
                     }
                  },
                  "1": {
                     "sum": {
                        "field": "A2"
                     }
                  },
                  "2": {
                     "sum": {
                        "field": "A3"
                     }
                  },
                  "3": {
                     "sum": {
                        "field": "A4"
                     }
                  },
                  "4": {
                     "sum": {
                        "field": "A5"
                     }
                  },
                  "5": {
                     "sum": {
                        "field": "A6"
                     }
                  },
                  "6": {
                     "sum": {
                        "field": "A7"
                     }
                  },
                  "7": {
                     "sum": {
                        "field": "A8"
                     }
                  }
               }

            }';
        }
        $params['body'] = $json1;

            try{
                $results = $this->_ES->search($params);

            }
            catch ( Exception  $e) {
                $erMsg = json_decode($e->getMessage(),true);

                $stCode = $erMsg['status'];
                return $stCode;
            }
            if(!empty($fromDate) && !empty($toDate)) {
                unset($results['aggregations']['final_bucket']['doc_count']);
                $ar = $results['aggregations']['final_bucket'];

                $interval   = array (
                    '0-3',
                    '3-6',
                    '3-6',
                    '6-9',
                    '9-12',
                    '12-15',
                    '15-18',
                    '18-21',
                    '21-24'
                );

                for ($i=0; $i<count($ar); $i++) {
                    $final[$i]  =  array(
                        'interval'  =>  $interval[$i],
                        'data'  => $ar[$i]['value']

                    );
                }

            } else {
                $ar = $results['aggregations'];
                $interval   = array (
                    '0-3',
                    '3-6',
                    '3-6',
                    '6-9',
                    '9-12',
                    '12-15',
                    '15-18',
                    '18-21',
                    '21-24'
                );

                for ($i=0; $i<count($ar); $i++) {
                    $final[$i]  =  array(
                        'interval'  =>  $interval[$i],
                        'data'  => $ar[$i]['value']

                    );
                }

            }
            $cnt = count($ar);
            if($cnt<=0) return 0;

            return $final;
        }

    function activeAppFieldTotal($fromDate=NULL,$toDate=NULL){

        $typeList = getIndexTypes('mysf_appgenlog');
        $params = array();
        $params['index'] = 'mysf_appgenlog';
        $typep = 'app1';


        foreach($typeList as  $type) {
            if(!empty($typep))$params['type'] = $type;
            if(!empty($fromDate) && !empty($toDate)) {
                $json1 = '
                {
               "size": 0,
               "aggs": {
                  "final_bucket": {
                     "filter": {
                        "range": {
                           "RU": {
                               "from": "'.$fromDate.'",
                                "to": "'.$toDate.'"
                           }
                        }
                     },
                     "aggs": {
                        "0-3": {
                           "sum": {
                              "field": "A1"
                           }
                        },
                        "3-6": {
                           "sum": {
                              "field": "A2"
                           }
                        },
                        "6-9": {
                           "sum": {
                              "field": "A3"
                           }
                        },
                        "9-12": {
                           "sum": {
                              "field": "A4"
                           }
                        },
                        "12-15": {
                           "sum": {
                              "field": "A5"
                           }
                        },
                        "15-18": {
                           "sum": {
                              "field": "A6"
                           }
                        },
                        "18-21": {
                           "sum": {
                              "field": "A7"
                           }
                        },
                        "21-24": {
                           "sum": {
                              "field": "A8"
                           }
                        }
                     }
                  }
               }

            }';
            }
            else
            {
                $json1 = '
                {
               "size": 0,
               "aggs": {
                  "0-3": {
                     "sum": {
                        "field": "A1"
                     }
                  },
                  "3-6": {
                     "sum": {
                        "field": "A2"
                     }
                  },
                  "6-9": {
                     "sum": {
                        "field": "A3"
                     }
                  },
                  "9-12": {
                     "sum": {
                        "field": "A4"
                     }
                  },
                  "12-15": {
                     "sum": {
                        "field": "A5"
                     }
                  },
                  "15-18": {
                     "sum": {
                        "field": "A6"
                     }
                  },
                  "18-21": {
                     "sum": {
                        "field": "A7"
                     }
                  },
                  "21-24": {
                     "sum": {
                        "field": "A8"
                     }
                  }
               }

            }';
            }
            $params['body'] = $json1;
            try{
                $results = $res->search($params);
            }
            catch ( Exception  $e) {
                $erMsg = json_decode($e->getMessage(),true);
                $stCode = $erMsg['status'];
                return $stCode;
            }
            if(!empty($fromDate) && !empty($toDate)) {
                $ar = $results['aggregations']['final_bucket'];
            } else {
                $ar = $results['aggregations'];
            }
            $cnt = count($ar);
            if($cnt<=0) return 0;
        }
        return $ar;
    }

    function getCountByProject($fromDate=NULL,$toDate=NULL) {

        $params = array();
        $params['index']    =   'mysf_appgenlog';
        $params['type']     =   'app1';

        if(!empty($fromDate) && !empty($toDate)) {
            $json1 = '{
                "aggs": {
              "final_bucket": {
                 "filter": {
                    "range": {
                       "RU": {
                          "gte": "'.$fromDate.'",
                          "lte": "'.$toDate.'"
                       }
                    }
                 },
                 "aggs": {
                    "projectcount": {
                       "terms": {
                          "field": "_type"
                       }

                    }
                 }
              }
           }

        }';
                } else {
                    $json1 = '{
                 "aggs" : {
                "projectcount" : {
                    "terms" : {
                        "field" : "_type",
                        "size" : "0"

                    }
                }
            }

        }';
        }
        $params['body'] = $json1;
        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
        if(!empty($fromDate) && !empty($toDate)) {
            $finalResults = $results['aggregations']['final_bucket']['projectcount']['buckets'];
        } else {
            $finalResults = $results['aggregations']['projectcount']['buckets']; }
        return $finalResults;
    }

    function application_data_transfer($fromDate=NULL,$toDate=NULL)
    {
        $params = array();
        $params['index']  = 'mysf_appdatalog';
        $params['type']   = 'app2';

        if(!empty($fromDate) && !empty($toDate)) {
            $json1 = '{
                 "size": 0,
               "aggs": {
                  "final_bucket": {
                     "filter": {
                        "range": {
                           "RU": {
                               "gte": "'.$fromDate.'",
                              "lte": "'.$toDate.'"
                           }
                        }
                     },
                     "aggs": {
                        "count_by_field1": {
                           "terms": {
                              "field": "AP",
                              "size" :"0",
                              "order" : { "avgtrans.value" : "desc", "avgrec.value " : "desc"}
                           },
                           "aggs": {
                              "avgtrans": {
                                 "sum": {
                                    "field": "AT"
                                 }
                              },
                              "avgrec": {
                                 "sum": {
                                    "field": "AR"
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }';
        } else {
            $json1 = '{
                  "size": 0,
               "aggs": {
                  "count_by_field1": {
                     "terms": {
                        "field": "AP",
                        "size" :"0",
                        "order" : { "avgtrans.value" : "desc", "avgrec.value " : "desc"}
                     },
                     "aggs": {
                        "avgtrans": {
                           "sum": {
                              "field": "AT"
                           }
                        },
                        "avgrec": {
                           "sum": {
                              "field": "AR"
                           }
                        }
                     }
                  }
               }

            }';
        }
        $params['body'] = $json1;
        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
        if(!empty($fromDate) && !empty($toDate)) {
            $finalResults = $results['aggregations']['final_bucket']['count_by_field1']['buckets'];
        } else {
            $finalResults = $results['aggregations']['count_by_field1']['buckets'];
        }

        $count = count($finalResults);
        if($count<=0) return 0;
        for($m=0;$m<$count;$m++){
            if($finalResults[$m]['avgtrans']['value'] > 6000 && $finalResults[$m]['avgrec']['value'] > 6000)
                $frAr[$finalResults[$m]['key']] = array(number_format($finalResults[$m]['avgtrans']['value'],0,'',''),number_format($finalResults[$m]['avgrec']['value'],0,'',''));
        }
        return $frAr;
    }

    function getIndexTypes($index)
    {

        $r=$this->_ES->indices()->getMapping();
        $results = $r[$index]['mappings'];
        $count = count($results);
        if($count<=0) return 0;
        foreach($results as $key=>$val)
        {
            $types[] = $key;
        }
        return $types;
    }

    function getAppDataDailyAvg($fromDate=NULL,$toDate=NULL)
    {
        $params = array();
        $params['index']  = 'mysf_appdatalog';
        $params['type']   = 'app2';
        if(!empty($fromDate) && !empty($toDate)) {
            $json1 = '{
                 "size": 0,
               "aggs": {
                  "final_bucket": {
                     "filter": {
                        "range": {
                           "RU": {
                               "gte": "'.$fromDate.'",
                              "lte": "'.$toDate.'"
                           }
                        }
                     },
                     "aggs": {
                        "count_by_field1": {
                           "terms": {
                              "field": "AP",
                              "size" :"0",
                              "order" : { "avgtrans.value" : "desc", "avgrec.value " : "desc"}
                           },
                           "aggs": {
                              "avgtrans": {
                                 "sum": {
                                    "field": "AT"
                                 }
                              },
                              "avgrec": {
                                 "sum": {
                                    "field": "AR"
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }';
        } else {
            $json1 = '{
                  "size": 0,
               "aggs": {
                  "count_by_field1": {
                     "terms": {
                        "field": "AP",
                        "size" :"0",
                        "order" : { "avgtrans.value" : "desc", "avgrec.value " : "desc"}
                     },
                     "aggs": {
                        "avgtrans": {
                           "sum": {
                              "field": "AT"
                           }
                        },
                        "avgrec": {
                           "sum": {
                              "field": "AR"
                           }
                        }
                     }
                  }
               }

            }';
        }
        $params['body'] = $json1;
        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
        if(!empty($fromDate) && !empty($toDate)) {
            $finalResults = $results['aggregations']['final_bucket']['count_by_field1']['buckets'];
        } else {
            $finalResults = $results['aggregations']['count_by_field1']['buckets'];
        }

        $count = count($finalResults);
        if($count<=0) return 0;
        for($m=0;$m<$count;$m++){
            if($finalResults[$m]['avgtrans']['value'] > 6000 && $finalResults[$m]['avgrec']['value'] > 6000)
                $frAr[$finalResults[$m]['key']] = array(number_format($finalResults[$m]['avgtrans']['value'],0,'',''),number_format($finalResults[$m]['avgrec']['value'],0,'',''));
        }
        return $frAr;

    }

    function getAppTotalDuration($fromDate=NULL,$toDate=NULL)
    {
        $params = array();
        $params['index']  = 'mysf_appgenlog';
        $params['type']   = 'app1';
        if(!empty($fromDate) && !empty($toDate)) {
            $json1 = '{
               "size": 0,
           "aggs": {
              "final_bucket": {
                 "filter": {
                    "range": {
                       "RU": {
                          "gte": "'.$fromDate.'",
                          "lte": "'.$toDate.'"
                       }
                    }
                 },
                 "aggs": {
                    "count_by_field1": {
                       "terms": {
                          "field": "AP",
                          "size" : "0",
                          "order" : { "dusum.value" : "desc"}
                       },
                       "aggs": {
                          "dusum": {
                             "sum": {
                                "field": "DU"
                             }
                          }
                       }
                    }
                 }
              }
           }
        }';
        } else {
            $json1 = '{
	        "size": 0,
               "aggs": {
                  "count_by_field1": {
                     "terms": {
                        "field": "AP",
                         "size" : "0",
                         "order" : { "dusum.value" : "desc"}
                     },
                     "aggs": {
                        "dusum": {
                           "sum": {
                              "field": "DU"
                           }
                        }
                     }
                  }
               }

            }';
        }
        $params['body'] = $json1;
        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
        if(!empty($fromDate) && !empty($toDate)) {
            $finalResults = $results['aggregations']['final_bucket']['count_by_field1']['buckets'];
        } else {
            $finalResults = $results['aggregations']['count_by_field1']['buckets'];
        }
        $count = count($finalResults);
        if($count<=0) return 0;
        for($m=0;$m<$count;$m++){
            $frAr[$finalResults[$m]['key']] = $finalResults[$m]['dusum']['value'];
        }
        return $frAr;

    }

    function getTopVisitedUrl($fromDate=NULL,$toDate=NULL) {

        $params = array();
        $params['index']  = 'mysf_urldatalog';
        $params['type']   = 'url';
        if(!empty($fromDate) && !empty($toDate)){
            $json1 = '{
           "size": 0,
           "aggs": {
              "final_bucket": {
                 "filter": {
                    "range": {
                       "RU": {
                          "gte": "'.$fromDate.'",
                          "lte": "'.$toDate.'"
                       }
                    }
                 },
                 "aggs": {
                    "voucher": {
                       "terms": {
                          "field": "UR",
                          "size": 0
                       }
                    }
                 }
              }
           }

        }';

        }
        else {
            $json1 = '{
               "size": 0,
                  "aggs": {
                    "voucher": {
                      "terms": {
                        "field": "UR",
                        "size": 0
                      }
                    }
              }

            }';
        }
        $params['body'] = $json1;
        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
        if(!empty($fromDate) && !empty($toDate)){
            $ar = $results['aggregations']['final_bucket']['voucher']['buckets'];
        } else {
            $ar = $results['aggregations']['voucher']['buckets'];
        }
        $cnt = count($ar);
        if($cnt<=0) return 0;
        for($i=0;$i<$cnt;$i++){
            $myKey = $this->keywordsFinalstring($ar[$i]['key']);
            $finalResult[$myKey] = $ar[$i]['doc_count'];
        }
        return $finalResult;
    }

    function keywordsFinalstring($str){
        $pos = strpos($str,"_");
        return $finalStr = ($pos===false)? ucwords($str): ucwords(str_replace("_"," ",$str));

    }
}