<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboards
{

    //initial variable
    private $_CI;
    private $_ES;
    private $_HOST;

    //construct 
    public function __construct()
    {

        $this->_CI =& get_instance();
        $this->_CI->load->helper('url');

        $this->_HOST = $this->_CI->config->item('ES_HOST');

        $this->_ES = Elasticsearch\ClientBuilder::create()
            ->setHosts($this->_HOST)
            ->setRetries(0)
            ->build();

    }

    public function dashboardUser($from = NULL, $to = NULL)
    {

        $index = "clientdata";
        $params = array();
        $params['index'] = $index;


        $json = '{
                    "size" : 0,
                    "aggs" : {
                        "uniq_user" : {
                            "cardinality" : {
                              "field" : "RW.zid"
                            }
                        }
                    }
                }';

        $finalResult = '';
        $params['body'] = $json;

        try {
            $results = $this->_ES->search($params);
        } catch (Exception  $e) {
            $erMsg = json_decode($e->getMessage(), true);
            $stCode = $erMsg['status'];
            return $stCode;
        }
        return $results;
    }

    public function dashboardCampaign($currency, $from=NULL, $to=NULL, $price_type=NULL, $client_id=NULL) {

        $index = "mysf_campaigndata";
        $type = "cam";
        $params = array();
        $params['index'] = $index;
        $params['type']   = $type;
        $string = array();
        $camptype="0";

        $string[] = '{ "match": {  "currency": "'.$currency.'" } }';

        if($camptype !=null)
            $string[] ='{ "match": {  "camptype": "'.$camptype.'" } }';

        if ($price_type)
            $string[] ='{ "match": {  "price_type": "'.$price_type.'" } }';

        if ($client_id)
            $string[] = '{  "terms":{ "client_id":['.$client_id.'] } }';

        if(!empty($from) && !empty($to))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$from.'", "lte":"'.$to.'" } } } ';

        $final_str='';
        if(count($string) > 0)
        {
            $final_str = '"query":{"bool":{"must":['.implode(',',$string).']}';
        }

        $json1 = '{"size": 0,
            '.$final_str.'
           },
               "aggs": { "amk_share": {
                     "sum": { "script": "_source.price * _source.clicks * (_source.DD.TAG.AMK_SHARE) * 0.01" }
               },"sf_share": {
                             "sum": {
                               "script": "_source.price * _source.clicks * (_source.DD.TAG.SF_SHARE.value) * 0.01"
                             }
                          }
            }
         }
       }';

        $finalResult='';
        $params['body'] = $json1;

        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];

            return $stCode;

        }
        return $results;
    }

    /* dashboard campaign chart
       return same with daily report revenue
       created : 2016-03-13
       @author : amier
    */
    public function dashboardCampaignCharts($currency, $from=NULL, $to=NULL,$price_type=NULL, $client_id=NULL) {

        $index = "mysf_campaigndata";
        $type = "cam";
        $params = array();
        $params['index'] = $index;
        $params['type']   = $type;
        $string = array();
        $camptype="0";
        //$size = totalDocument('mysf_campaigndata');

        $string[] = '{ "match": {  "currency": "'.$currency.'" } }';

        if($camptype !=null)
            $string[] ='{ "match": {  "camptype": "'.$camptype.'" } }';

        if ($price_type)
            $string[] ='{ "match": {  "price_type": "'.$price_type.'" } }';

        if ($client_id)
            $string[] = '{  "terms":{ "client_id":['.$client_id.'] } }';

        if(!empty($from) && !empty($to))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$from.'", "lte":"'.$to.'" } } } ';

        $final_str='';

        if(!empty($fromDate) && !empty($toDate))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$from.'", "lte":"'.$to.'" } } } ';

        $final_str='';
        if(count($string) > 0)
        {
            $final_str = '"query":{"bool":{"must":['.implode(',',$string).']}';
        }

        $json1 = '{"size": 0,
            '.$final_str.'
           },

           "aggs": {
                    "monthly": {
                       "date_histogram": {
                          "field": "DD.ST",
                          "min_doc_count":0,
                          "interval": "day",
                          "format": "yyyy-MM-dd"
                       },
                       "aggs": {
                          "priceXclicks": {
                             "sum": {
                                "script":"_source.price * _source.clicks"
                             }
                          },
                           "send": {
                             "sum": {
                              "field": "events.SEND"
                             }
                          },
                          "received": {
                             "sum": {
                              "field": "events.RECEIVED"
                             }
                          },
                          "viwed": {
                             "sum": {
                              "field": "events.VIEWED"
                             }
                          },
                           "price": {
                             "sum": {
                              "field": "price"
                             }
                          },
                           "clicks": {
                             "sum": {
                              "field": "clicks"
                             }
                          },
                           "sf_share": {
                             "sum": {
                               "script": "_source.price * _source.clicks * (_source.DD.TAG.SF_SHARE.value)"
                             }
                          },
                           "amk_share": {
                             "sum": {
                               "script": "_source.price * _source.clicks * (_source.DD.TAG.AMK_SHARE)"
                             }
                          }
                       }
                    }
                 }
           }';

        $finalResult='';
        $params['body'] = $json1;

        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];

            return $stCode;

        }

        $cnt = count($results['aggregations']['monthly']['buckets']);
        $final ='';

        if($cnt>0){
            $finalResult = $results['aggregations']['monthly']['buckets'];

            for ($i=0; $i < count($finalResult); $i++ ) {

                $final[$i] = array(
                    "date"          => date("Y-m-d", strtotime($finalResult[$i]['key_as_string'])),
                    "sent"          =>$finalResult[$i]['send']['value'],
                    "received"      =>$finalResult[$i]['received']['value'],
                    "Clicks"        =>$finalResult[$i]['clicks']['value']
                );
            }
        }

        return $final;
    }
    /*
     * dashboard campaign chart ends
     */
    public function dashboardRevenueCharts($currency, $from=NULL, $to=NULL,$price_type=NULL, $client_id=NULL) {

        $index = "mysf_campaigndata";
        $type = "cam";
        $params = array();
        $params['index'] = $index;
        $params['type']   = $type;
        $string = array();
        $camptype="0";
        //$size = totalDocument('mysf_campaigndata');

        $string[] = '{ "match": {  "currency": "'.$currency.'" } }';

        if($camptype !=null)
            $string[] ='{ "match": {  "camptype": "'.$camptype.'" } }';

        if ($price_type)
            $string[] ='{ "match": {  "price_type": "'.$price_type.'" } }';

        if ($client_id)
            $string[] = '{  "terms":{ "client_id":['.$client_id.'] } }';

        if(!empty($from) && !empty($to))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$from.'", "lte":"'.$to.'" } } } ';

        $final_str='';

        if(!empty($fromDate) && !empty($toDate))
            $string[] = '{ "range": { "DD.ST": { "gte":"'.$from.'", "lte":"'.$to.'" } } } ';

        $final_str='';
        if(count($string) > 0)
        {
            $final_str = '"query":{"bool":{"must":['.implode(',',$string).']}';
        }

        $json1 = '{"size": 0,
            '.$final_str.'
           },

           "aggs": {
                    "monthly": {
                       "date_histogram": {
                          "field": "DD.ST",
                          "min_doc_count":0,
                          "interval": "day",
                          "format": "yyyy-MM-dd"
                       },
                       "aggs": {
                          "priceXclicks": {
                             "sum": {
                                "script":"_source.price * _source.clicks"
                             }
                          },
                           "sf_share": {
                             "sum": {
                               "script": "_source.price * _source.clicks * (_source.DD.TAG.SF_SHARE.value) * 0.01"
                             }
                          },
                           "amk_share": {
                             "sum": {
                               "script": "_source.price * _source.clicks * (_source.DD.TAG.AMK_SHARE) * 0.01"
                             }
                          }
                       }
                    }
                 }
           }';

        $finalResult='';
        $params['body'] = $json1;

        try{
            $results = $this->_ES->search($params);
        }
        catch ( Exception  $e) {
            $erMsg = json_decode($e->getMessage(),true);
            $stCode = $erMsg['status'];

            return $stCode;

        }

        $cnt = count($results['aggregations']['monthly']['buckets']);
        $final ='';

        if($cnt>0){
            $finalResult = $results['aggregations']['monthly']['buckets'];

            for ($i=0; $i < count($finalResult); $i++ ) {

                $final[$i] = array(
                    "date"     => date("Y-m-d", strtotime($finalResult[$i]['key_as_string'])),
                    "amk"   =>$finalResult[$i]['amk_share']['value'],
                    "sf"    =>$finalResult[$i]['sf_share']['value'],
                    "Total"   =>$finalResult[$i]['priceXclicks']['value']
                );
            }
        }

        return $final;
    }

    public function totalDocument($index=NULL)
    {
        $index = (!empty($indexp)) ? $indexp : $index;
        $totCnt = $this->_ES->count(array('index' => $index));
        return $totCnt['count'];
    }
}