<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WalletHandler {
    private $_CI;
    
    /**
     * Constructor for Wallet Handler library
     * 
     * @return void
     */
    public function __construct() {
        $this->_CI =& get_instance();
        $this->_CI->load->model('member_model', 'member_model');
        $this->_CI->load->model('member_wallet_model', 'member_wallet_model');
    }

    /**
     * Method get_wallet_balance
     * Used to get member's wallet balance
     * 
     * @param int $member_id 
     * @return array
     */
    public function get_wallet_balance($member_id)
    {
        return $this->member_wallet_model->get_wallet_balance($member_id);
    }
}
