function dashboard() {

        $('#reportrange').daterangepicker({
                opens: 'left',
                startDate: moment().subtract('days', 29),
                endDate: moment(),
                minDate: '02/01/2016',
                maxDate: '01/01/2017',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                buttonClasses: ['btn'],
                applyClass: 'green',
                cancelClass: 'default',
                format: 'MM/DD/YYYY',
                separator: ' to '
            },

            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                startDate   =   start.format('YYYY-MM-DD');
                endDate     =   end.format('YYYY-MM-DD');

                Metronic.startPageLoading({animate: true});
                $.ajax({
                    url: domain+"dashboard/chart_campaign_data",
                    type: 'POST',
                    data: {sd:startDate, ed:endDate},
                    dataType: 'json',
                    method: 'post',
                    success: function(result) {
                        makeChartRevenue(result.chart_revenue);
                        makeChartCampaign(result.chart_traffic);
                        $('#amk_revenue_idr').html(addCommas(result.amk_revenue_idr));
                        $('#amk_revenue_usd').html(addCommas(result.amk_revenue_usd));
                        $('#sf_revenue_idr').html(addCommas(result.sf_revenue_idr));
                        $('#sf_revenue_usd').html(addCommas(result.sf_revenue_usd));
                    },
                    complete: function() {
                        Metronic.stopPageLoading();
                    }
                })
        }
        );

        //important! : Set the initial state of the picker label and ajax request
        fromDate    =   moment().subtract('days', 29);
        toDate      =   moment();

        $('#reportrange span').html(fromDate.format('MMMM D, YYYY') + ' - ' +toDate.format('MMMM D, YYYY'));

        from   = fromDate.format('YYYY-MM-DD');
        to     = toDate.format('YYYY-MM-DD');

        Metronic.startPageLoading({animate: true});
        $.ajax({
            url: domain+"dashboard/chart_campaign_data",
            type: 'POST',
            data: {sd:from, ed:to},
            dataType: 'json',
            method: 'post',
            success: function(result) {
                makeChartRevenue(result.chart_revenue);
                makeChartCampaign(result.chart_traffic);
                $('#amk_revenue_idr').html(addCommas(result.amk_revenue_idr));
                $('#amk_revenue_usd').html(addCommas(result.amk_revenue_usd));
                $('#sf_revenue_idr').html(addCommas(result.sf_revenue_idr));
                $('#sf_revenue_usd').html(addCommas(result.sf_revenue_usd));
            },
            complete: function() {
                Metronic.stopPageLoading();
            }
        })
    }

    function makeChartRevenue(dataRevenue) {
        var chart = AmCharts.makeChart("chart_12", {
            "type": "serial",
            "theme": "light",

            "dataProvider" : dataRevenue,

            "fontFamily": 'Open Sans',
            "color":    '#888',

            "valueAxes": [{
                "id": "totalValue",
                "axisAlpha": 0,
                "position": "left"
            }],
            // "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Total Revenue",
                "type": "column",
                "valueField": "Total",
                "valueAxis": "totalValue"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "dashLengthField": "dashLengthLine",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Amk Revenue",
                "valueField": "amk",
                "valueAxis": "totalValue"
            },{
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "dashLengthField": "dashLengthLine",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "SF Revenue",
                "valueField": "sf",
                "valueAxis": "totalValue"
            }],
            "dataDateFormat": "YYYY-MM-DD",
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "dateFormats": [{
                    "period": "DD",
                    "format": "MMM DD"
                }, {
                    "period": "WW",
                    "format": "MMM DD"
                }, {
                    "period": "MM",
                    "format": "MMM"
                }, {
                    "period": "YYYY",
                    "format": "YYYY"
                }],
                "parseDates": true,
                autoGridCount : false,
                "axisAlpha": 0
            }
        });

        $('#chart_1').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    function makeChartCampaign(dataCampaign) {

    var chart = AmCharts.makeChart("chart_traffic", {
        "type": "serial",
        "theme": "light",
        "fontFamily": 'Open Sans',
        "color":    '#888888',

        "dataProvider" : dataCampaign,

        "legend": {
            "equalWidths": false,
            "useGraphSettings": true,
            "valueAlign": "left",
            "valueWidth": 80
        },

        "valueAxes": [{
            "id": "revenueAxis",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left",
            "title": "Sent"
        }],
        "graphs": [{
            "alphaField": "alpha",
            "balloonText": "Sent : [[value]]",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.7,
            "legendPeriodValueText": "total: [[value.sum]]",
            "legendValueText": "[[value]]",
            "title": "Sent",
            "type": "column",
            "valueField": "sent",
            "valueAxis": "sentAxis"
        }, {
            "balloonText": "received:[[value]]",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "useLineColorForBulletBorder": true,
            "bulletColor": "#FFFFFF",
            "bulletSizeField": "townSize",
            "dashLengthField": "dashLength",
            "descriptionField": "townName",
            "labelPosition": "right",
            "labelText": "[[received]]",
            "legendValueText": "[[value]]",
            "title": "received",
            "fillAlphas": 0,
            "valueField": "received",
            "valueAxis": "receivedAxis"
        }, {
            "balloonText": "Clicks:[[value]]",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]",
            "title": "Clicks",
            "fillAlphas": 0,
            "valueField": "Clicks",
            "valueAxis": "ClicksAxis"
        }],
        "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor": "#000000",
            "fullWidth": true,
            "valueBalloonsEnabled": false,
            "zoomable": false
        },
        "dataDateFormat": "YYYY-MM-DD",
        "categoryField": "date",
        "startDuration": 1,
        "rotate": false,

        "categoryAxis": {
            "dateFormats": [{
                "period": "DD",
                "format": "DD"
            }, {
                "period": "WW",
                "format": "MMM DD"
            }, {
                "period": "MM",
                "format": "MMM"
            }, {
                "period": "YYYY",
                "format": "YYYY"
            }],

            "parseDates": true,
            "autoGridCount": false,
            "axisColor": "#555555",
            "gridAlpha": 0.1,
            "gridColor": "#FFFFFF",
            "gridCount": 50
        },
        "exportConfig": {
            "menuBottom": "20px",
            "menuRight": "22px",
            "menuItems": [{
                "icon": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                "format": 'png'
            }]
        }
    });

    $('#chart_campaign').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });
}

