/**
 * Created by amir on 21/03/2016.
 */

function finance() {

    $('#financerange').daterangepicker({
            opens: 'left',
            startDate: moment().subtract('days', 29),
            endDate: moment(),
            minDate: '02/01/2016',
            maxDate: '01/01/2017',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'MM/DD/YYYY',
            separator: ' to '
        },

        //
        function (start, end) {
            $('#financerange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            $('#from').val(start.format('YYYY-MM-DD'));
            $('#to').val(end.format('YYYY-MM-DD'));

            var startDate = $('#from').val();
            var endDate =   $('#to').val();
            var priceType = $('#price_type').val();
            campaign_type = $('#campaign_type').val();

            if (data == "") {
                var currency = NULL;
            } else {
                var currency =  $('#currency').val();
            }

            Metronic.startPageLoading({animate: true});
            $.ajax({
                url: domain + "financial/chart_daily_usd",
                type: 'POST',
                data: {curr:currency, sd: startDate, ed: endDate, pt: priceType, ct:campaign_type},
                dataType: 'json',
                method: 'post',
                success: function (result) {
                        createChartDailyUSD(result.usd);
                        createDatatables(result.usd);
                        createSummaryType(result.summary);
                        createChartDailyIDR(result.idr);
                        createDatatablesIDR(result.idr);

                        createChartMonthly(result.monthly);
                        createTableText(result.text);
                        createTableApk(result.apk);
                        createTableAudio(result.audio);
                        createTableVideo(result.video);
                        createTableImage(result.image);
                        createTableShortcut(result.shortcut);
                        createTableCampaign(result.campaign);
                },
                complete: function() {
                    Metronic.stopPageLoading();
                }
            })



        });

        $('#price_type').change(function () {
            var start = $('#from').val();
            var end   = $('#to').val();
            pricetype = $('#price_type').val();
            campaign_type = $('#campaign_type').val();

            var data = $('#currency').val();

            if (data == "") {
                var currency = NULL;
            } else {
                var currency =  $('#currency').val();
            }

            Metronic.startPageLoading({animate: true});
            $.ajax({
                url: domain + "financial/chart_daily_usd",
                type: 'POST',
                data: {curr: currency, sd: start, ed: end, pt: pricetype, ct:campaign_type},
                dataType: 'json',
                method: 'post',
                success: function (result) {

                        createChartDailyUSD(result.usd);
                        createDatatables(result.usd);

                        createChartDailyIDR(result.idr);
                        createDatatablesIDR(result.idr);

                        createChartMonthly(result.monthly);
                        createSummaryType(result.summary);
                        createTableText(result.text);
                        createTableApk(result.apk);
                        createTableAudio(result.audio);
                        createTableVideo(result.video);
                        createTableImage(result.image);
                        createTableShortcut(result.shortcut);
                    },
                complete: function() {
                    Metronic.stopPageLoading();
                }
            })
         });

    $('#currency').change(function () {
        var start = $('#from').val();
        var end   = $('#to').val();
        pricetype = $('#price_type').val();
        currency  = $('#currency').val();
        campaign_type = $('#campaign_type').val();

        Metronic.startPageLoading({animate: true});
        $.ajax({
            url: domain + "financial/chart_daily_usd",
            type: 'POST',
            data: {curr: currency, sd: start, ed: end, pt: pricetype, ct:campaign_type},
            dataType: 'json',
            method: 'post',
            success: function (result) {

                createChartMonthly(result.monthly);
                createSummaryType(result.summary);
                createTableText(result.text);
                createTableApk(result.apk);
                createTableAudio(result.audio);
                createTableVideo(result.video);
                createTableImage(result.image);
                createTableShortcut(result.shortcut);


            },
            complete: function() {
                Metronic.stopPageLoading();
            }
        })
    });

    $('#campaign_type').change(function () {
        var start = $('#from').val();
        var end   = $('#to').val();
        pricetype = $('#price_type').val();
        currency  = $('#currency').val();
        campaign_type = $('#campaign_type').val();

        Metronic.startPageLoading({animate: true});
        $.ajax({
            url: domain + "financial/chart_daily_usd",
            type: 'POST',
            data: {curr: currency, sd: start, ed: end, pt: pricetype, ct:campaign_type},
            dataType: 'json',
            method: 'post',
            success: function (result) {

                createChartMonthly(result.monthly);
                createSummaryType(result.summary);
                createTableText(result.text);
                createTableApk(result.apk);
                createTableAudio(result.audio);
                createTableVideo(result.video);
                createTableImage(result.image);
                createTableShortcut(result.shortcut);
            },
            complete: function() {
                Metronic.stopPageLoading();
            }
        })
    });


    fromDate    =   moment().subtract('days', 29);
    toDate      =   moment();

    $('#financerange span').html(fromDate.format('MMMM D, YYYY') + ' - ' +toDate.format('MMMM D, YYYY'));

    $('#from').val(fromDate.format('YYYY-MM-DD'));
    $('#to').val(toDate.format('YYYY-MM-DD'));

    var startDate = $('#from').val();
    var endDate =   $('#to').val();
    var priceType = $('#price_type').val();
    campaign_type = $('#campaign_type').val();

    var data = $('#currency').val();

    if (data == "") {
        var currency = NULL;
    } else {
        var currency =  $('#currency').val();
    }

    //TODO: make click event to send parameter through ajax request
        Metronic.startPageLoading({animate: true});
        $.ajax({
            url: domain + "financial/chart_daily_usd",
            type: 'POST',
            data: {curr: currency, sd: startDate, ed: endDate, pt:priceType, ct:campaign_type},
            dataType: 'json',
            method: 'post',
            success: function (result) {
                    createChartDailyUSD(result.usd);
                    createDatatables(result.usd);

                    createChartDailyIDR(result.idr);
                    createDatatablesIDR(result.idr);

                    createChartMonthly(result.monthly);
                    createSummaryType(result.summary);
                    createTableText(result.text);
                    createTableApk(result.apk);
                    createTableAudio(result.audio);
                    createTableVideo(result.video);
                    createTableImage(result.image);
                    createTableShortcut(result.shortcut);
                    createTableCampaign(result.campaign);
            },
            complete: function() {
                Metronic.stopPageLoading();
            }

        })
}


function createChartDailyUSD(usdData) {
    var chart = AmCharts.makeChart("chart_daily_usd", {
        "type": "serial",
        "theme": "light",

        "fontFamily": 'Open Sans',
        "color":    '#888888',

        "legend": {
            "equalWidths": false,
            "useGraphSettings": true,
            "valueAlign": "left",
            "valueWidth": 80
        },
        "dataProvider": usdData,
        "valueAxes": [{
            "id": "revenueAxis",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left",
            "title": "revenue"
        }, {
            "id": "ClicksAxis",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "inside": true,
            "position": "right",
            "title": "Percentage"
        }],
        "graphs": [{
            "alphaField": "alpha",
            "balloonText": "[[value]] USD",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.7,
            "legendPeriodValueText": "total: [[value.sum]] USD",
            "legendValueText": "[[value]] USD",
            "title": "revenue",
            "type": "column",
            "valueField": "revenue",
            "valueAxis": "revenueAxis"
        }, {
            "balloonText": "received:[[value]]",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "useLineColorForBulletBorder": true,
            "bulletColor": "#FFFFFF",
            "bulletSizeField": "townSize",
            "dashLengthField": "dashLength",
            "descriptionField": "townName",
            "labelText": "[[received]]",
            "legendValueText": "[[value]]",
            "title": "Received",
            "fillAlphas": 0,
            "valueField": "receive_percentage",
            "valueAxis": "ClicksAxis"
        }, {
            "bullet": "square",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]",
            "title": "Display",
            "fillAlphas": 0,
            "valueField": "display_percentage",
            "valueAxis": "ClicksAxis"
        }, {
            "bullet": "square",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]",
            "title": "CTR",
            "fillAlphas": 0,
            "valueField": "ctr",
            "valueAxis": "ClicksAxis"
        }],
        "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor": "#000000",
            "fullWidth": true,
            "valueBalloonsEnabled": false,
            "zoomable": false
        },
        "dataDateFormat": "YYYY-MM-DD",
        "categoryField": "date",
        "categoryAxis": {
            "dateFormats": [{
                "period": "DD",
                "format": "DD"
            }, {
                "period": "WW",
                "format": "MMM DD"
            }, {
                "period": "MM",
                "format": "MMM"
            }, {
                "period": "YYYY",
                "format": "YYYY"
            }],
            "parseDates": true,
            "autoGridCount": false,
            "axisColor": "#555555",
            "gridAlpha": 0.1,
            "gridColor": "#FFFFFF",
            "gridCount": 50
        },
        "exportConfig": {
            "menuBottom": "20px",
            "menuRight": "22px",
            "menuItems": [{
                "icon": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                "format": 'png'
            }]
        }
    });

    $('#chart_2').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });
}

function createDatatables(dataDaily) {
    var table = $('#daily_usd');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "responsive": true,
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "decimal": ",",
            "thousands": "."
        },
        "data": dataDaily,

        columns: [
            {data: "date"},
            {data: "inventory",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "Price",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "served",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "received",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "receive_percentage",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
            {data: "impression",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "display_percentage",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
            {data: "clicks",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "ctr",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
            {data: "revenue",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
        ],


        "order": [
            [1, 'asc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,
        "paging": true,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        },

        footerCallback: function ( row, data, start, end, display ) {
            this.api().columns('.sum').every(function () {
                var column = this;
                var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                var sum = column
                    .data()
                    .reduce(function (a, b) {
                        return parseInt(a, 10) + parseInt(b, 10);
                    });

                $(column.footer()).html(numFormat(sum));
            });
        }

    })


    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown



}

function createChartDailyIDR(idrData) {
    var chart = AmCharts.makeChart("chart_daily_IDR", {
        "type": "serial",
        "theme": "light",

        "fontFamily": 'Open Sans',
        "color":    '#888888',

        "legend": {
            "equalWidths": false,
            "useGraphSettings": true,
            "valueAlign": "left",
            "valueWidth": 80
        },
        "dataProvider": idrData,
        "valueAxes": [{
            "id": "revenueAxis",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left",
            "title": "revenue"
        }, {
            "id": "ClicksAxis",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "inside": true,
            "position": "right",
            "title": "Percentage"
        }],
        "graphs": [{
            "alphaField": "alpha",
            "balloonText": "[[value]] IDR",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.7,
            "legendPeriodValueText": "total: [[value.sum]] IDR",
            "legendValueText": "[[value]] USD",
            "labelText": "[[revenue]]",
            "title": "revenue",
            "type": "column",
            "valueField": "revenue",
            "valueAxis": "revenueAxis"
        }, {
            "balloonText": "received:[[value]]",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "useLineColorForBulletBorder": true,
            "bulletColor": "#FFFFFF",
            "bulletSizeField": 1,
            "dashLengthField": 1,
            "descriptionField": "receive_percentage",

            "legendValueText": "[[value]]",
            "title": "Received",
            "fillAlphas": 0,
            "valueField": "receive_percentage",
            "valueAxis": "ClicksAxis"
        }, {
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "dashLengthField": "dashLength",
            //"labelText": "[[display_percentage]]",
            "legendValueText": "[[value]]",
            "title": "Display",
            "fillAlphas": 0,
            "valueField": "display_percentage",
            "valueAxis": "ClicksAxis"
        }, {
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]",
            "title": "CTR",
            "fillAlphas": 0,
            "valueField": "ctr",
            "valueAxis": "ClicksAxis"
        }],
        "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor": "#000000",
            "fullWidth": true,
            "valueBalloonsEnabled": false,
            "zoomable": false
        },
        "dataDateFormat": "YYYY-MM-DD",
        "categoryField": "date",
        "categoryAxis": {
            "dateFormats": [{
                "period": "DD",
                "format": "DD"
            }, {
                "period": "WW",
                "format": "MMM DD"
            }, {
                "period": "MM",
                "format": "MMM"
            }, {
                "period": "YYYY",
                "format": "YYYY"
            }],
            "parseDates": true,
            "autoGridCount": false,
            "axisColor": "#555555",
            "gridAlpha": 0.1,
            "gridColor": "#FFFFFF",
            "gridCount": 50
        },
        "exportConfig": {
            "menuBottom": "20px",
            "menuRight": "22px",
            "menuItems": [{
                "icon": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                "format": 'png'
            }]
        }
    });

    $('#chart_2').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });
}

function createDatatablesIDR(dataDaily) {
    var table = $('#daily_idr');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "responsive": true,
        "language": {
            "decimal": ",",
            "thousands": ".",
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"

        },
        "data": dataDaily,

        columns: [
            {data: "date"},
            {data: "inventory",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "Price",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "served",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "received",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "receive_percentage",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
            {data: "impression",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "display_percentage",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
            {data: "clicks",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "ctr",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
            {data: "revenue",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
        ],


        "columnDefs": [
            { type: 'date-dd-mmm-yyyy', targets: 0 }
        ],


        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [1, 'asc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,
        "paging": true,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        },

        footerCallback: function ( row, data, start, end, display ) {
            this.api().columns('.sum').every(function () {
                var column = this;
                var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                var sum = column
                    .data()
                    .reduce(function (a, b) {
                        return parseInt(a, 10) + parseInt(b, 10);
                    });

                $(column.footer()).html(numFormat(sum));
            });
        }

    })


    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

}

function createChartMonthly(dataMonthly) {
    var chart = AmCharts.makeChart("chart_monthly", {
        "type": "serial",
        "theme": "light",

        "fontFamily": 'Open Sans',
        "color":    '#888888',

        "pathToImages": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/",

        "dataProvider": dataMonthly,
        "balloon": {
            "cornerRadius": 6
        },
        "valueAxes": [{
            "id": "revenueAxis",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left",
            "title": "revenue"
        }],
        "graphs": [{
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "fillAlphas": 0.3,
            "fillColorsField": "lineColor",
            "legendValueText": "[[value]]",
            "labelText": "[[value]]",
            "lineColorField": "lineColor",
            "title": "Revenue",
            "valueField": "value",
            "valueAxis": "revenueAxis"

        }],
        "chartScrollbar": {},
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD",
            "cursorAlpha": 0,
            "zoomable": false
        },
        "dataDateFormat": "YYYY-MM-DD",
        "categoryField": "date",
        "categoryAxis": {
            "dateFormats": [{
                "period": "DD",
                "format": "DD"
            }, {
                "period": "WW",
                "format": "MMM DD"
            }, {
                "period": "MM",
                "format": "MMM"
            }, {
                "period": "YYYY",
                "format": "YYYY"
            }],
            "parseDates": true,
            "autoGridCount": false,
            "axisColor": "#555555",
            "gridAlpha": 0,
            "gridCount": 50
        }
    });

    $('#chart_monthly').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });
}

function createSummaryType(summaryData) {
    var table = $('#summary_type');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "responsive": true,
        "language": {
            "thousands": ".",
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"

        },
        "bLengthChange": false,
        "bPaginate": false,
        "bFilter": false,
        "data": summaryData,

        columns: [
            {data: "type"},
            {data: "price"},
            {data: "total_price", render: $.fn.dataTable.render.number( '\,', '.', 0, '' ) },
            {data: "click",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "budget",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "monthly_revenue",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
            {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
        ],
        "ordering": false,

        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ "_all" ] },
            { className: "dt-body-right", "targets": ["_all"] }
        ],

        "paging": false,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF",
                "sTitle": "mySF_Summary_campaign_"+created
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV",
                "sTitle": "mySF_Summary_campaign_"+created
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel",
                "sTitle": "mySF_Summary_campaign_"+created
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        },

        footerCallback: function ( row, data, start, end, display ) {
            this.api().columns('.sum').every(function () {
                var column = this;
                var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                var sum = column
                    .data()
                    .reduce(function (a, b) {
                        return parseInt(a, 10) + parseInt(b, 10);
                    });

                $(column.footer()).html(numFormat(sum));
            });
        }

    })

    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

}

function createTableText(textData) {

    if (textData == null) {
        $('#text').hide();
    } else {
        $('#text').show();
        var table = $('#summary_text');

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var oTable = table.dataTable({

            "responsive": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"

            },
            "bLengthChange": false,
            "bPaginate": false,
            "bFilter": false,
            "data": textData,

            columns: [
                {data: "name"},
                {data: "price_type"},
                {data: "price",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "click",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "budget",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "scaps",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "date"},
                {data: "status",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%')},
                {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "revenue_total",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "amk_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "sf_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
            ],

            "ordering": false,

            "aoColumnDefs": [
                {"bSortable": false, "aTargets": ["_all"]}
            ],

            "paging": false,
            "destroy": true,

            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel",
                    "sTitle": "mySF_Summary_campaign_" + created
                }, {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables"
                }]
            },

            footerCallback: function (row, data, start, end, display) {
                this.api().columns('.sum').every(function () {
                    var column = this;
                    var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                    var sum = column
                        .data()
                        .reduce(function (a, b) {
                            return parseInt(a, 10) + parseInt(b, 10);
                        });

                    $(column.footer()).html(numFormat(sum));
                });
            }

        })

        var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    }
}

function createTableApk(apkData) {

    if (apkData == null) {
        $('#apk').hide();
    } else {
        $('#apk').show();

        var table = $('#summary_apk');

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var oTable = table.dataTable({

            "responsive": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"

            },
            "bLengthChange": false,
            "bPaginate": false,
            "bFilter": false,
            "data": apkData,

            columns: [
                {data: "name"},
                {data: "price_type"},
                {data: "price",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "click",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "budget",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "scaps",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "date"},
                {data: "status",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%')},
                {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "revenue_total",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "amk_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "sf_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
            ],

            "order": [
                [1, 'asc']
            ],

            "aoColumnDefs": [
                {"bSortable": false, "aTargets": ["_all"]}
            ],

            "paging": false,
            "destroy": true,

            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel",
                    "sTitle": "mySF_Summary_campaign_" + created
                }, {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables"
                }]
            },

            footerCallback: function (row, data, start, end, display) {
                this.api().columns('.sum').every(function () {
                    var column = this;
                    var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                    var sum = column
                        .data()
                        .reduce(function (a, b) {
                            return parseInt(a, 10) + parseInt(b, 10);
                        });

                    $(column.footer()).html(numFormat(sum));
                });
            }

        })

        var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    }
}

function createTableAudio(audioData) {
    if (audioData == null) {
        $('#audio').hide();
    } else {
        $('#audio').show();

        var table = $('#summary_audio');

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var oTable = table.dataTable({

            "responsive": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"

            },
            "bLengthChange": false,
            "bPaginate": false,
            "bFilter": false,
            "data": audioData,

            columns: [
                {data: "name"},
                {data: "price_type"},
                {data: "price",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "click",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "budget",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "scaps",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "date"},
                {data: "status",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%')},
                {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "revenue_total",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "amk_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "sf_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
            ],

            "ordering": false,

            "aoColumnDefs": [
                {"bSortable": false, "aTargets": ["_all"]}
            ],

            "paging": false,
            "destroy": true,

            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel",
                    "sTitle": "mySF_Summary_campaign_" + created
                }, {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables"
                }]
            },

            footerCallback: function (row, data, start, end, display) {
                this.api().columns('.sum').every(function () {
                    var column = this;
                    var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                    var sum = column
                        .data()
                        .reduce(function (a, b) {
                            return parseInt(a, 10) + parseInt(b, 10);
                        });

                    $(column.footer()).html(numFormat(sum));
                });
            }

        })

        var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    }
}

function createTableVideo(videoData) {
    if (videoData == null) {
        $('#video').hide();
    } else {
        $('#video').show();

        var table = $('#summary_video');

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var oTable = table.dataTable({

            "responsive": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"

            },
            "bLengthChange": false,
            "bPaginate": false,
            "bFilter": false,
            "data": videoData,

            columns: [
                {data: "name"},
                {data: "price_type"},
                {data: "price",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "click",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "budget",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "scaps",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "date"},
                {data: "status",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%')},
                {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "revenue_total",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "amk_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "sf_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
            ],

            "ordering": false,

            "aoColumnDefs": [
                {"bSortable": false, "aTargets": ["_all"]}
            ],

            "paging": false,
            "destroy": true,

            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel",
                    "sTitle": "mySF_Summary_campaign_" + created
                }, {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables"
                }]
            },

            footerCallback: function (row, data, start, end, display) {
                this.api().columns('.sum').every(function () {
                    var column = this;
                    var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                    var sum = column
                        .data()
                        .reduce(function (a, b) {
                            return parseInt(a, 10) + parseInt(b, 10);
                        });

                    $(column.footer()).html(numFormat(sum));
                });
            }

        })

        var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    }
}

function createTableImage(imageData) {

    if (imageData == null) {
        $('#image').hide();
    } else {
        $('#image').show();

        var table = $('#summary_image');

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var oTable = table.dataTable({

            "responsive": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"

            },
            "bLengthChange": false,
            "bPaginate": false,
            "bFilter": false,
            "data": imageData,

            columns: [
                {data: "name"},
                {data: "price_type"},
                {data: "price",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "click",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "budget",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "scaps",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "date"},
                {data: "status",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%')},
                {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "revenue_total",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "amk_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "sf_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
            ],

            "ordering": false,

            "aoColumnDefs": [
                {"bSortable": false, "aTargets": ["_all"]}
            ],

            "paging": false,
            "destroy": true,

            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel",
                    "sTitle": "mySF_Summary_campaign_" + created
                }, {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables"
                }]
            },

            footerCallback: function (row, data, start, end, display) {
                this.api().columns('.sum').every(function () {
                    var column = this;
                    var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                    var sum = column
                        .data()
                        .reduce(function (a, b) {
                            return parseInt(a, 10) + parseInt(b, 10);
                        });

                    $(column.footer()).html(numFormat(sum));
                });
            }

        })

        var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    }
}

function createTableShortcut(shortcutData) {

    if (shortcutData == null) {
        $('#shortcut').hide();
    } else {
        $('#shortcut').show();

        var table = $('#summary_shortcut');

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var oTable = table.dataTable({

            "responsive": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"

            },
            "bLengthChange": false,
            "bPaginate": false,
            "bFilter": false,
            "bAutoWidth": false,
            "data": shortcutData,

            columns: [
                {data: "name"},
                {data: "price_type"},
                {data: "price",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "click",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "budget",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "scaps",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "date"},
                {data: "status",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%')},
                {data: "sf_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "amk_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '','%' )},
                {data: "revenue_total",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "amk_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )},
                {data: "sf_revenue_share",render: $.fn.dataTable.render.number( '\,', '.', 0, '' )}
            ],

            "ordering": false,

            "aoColumnDefs": [
                {"bSortable": false, "aTargets": ["_all"]}
            ],

            "paging": false,
            "destroy": true,

            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel",
                    "sTitle": "mySF_Summary_campaign_" + created
                }, {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables"
                }]
            },

            footerCallback: function (row, data, start, end, display) {
                this.api().columns('.sum').every(function () {
                    var column = this;
                    var numFormat = $.fn.dataTable.render.number( '\,', ',', '' ).display;
                    var sum = column
                        .data()
                        .reduce(function (a, b) {
                            return parseInt(a, 10) + parseInt(b, 10);
                        });

                    $(column.footer()).html(numFormat(sum));
                });
            }

        })

        var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    }
}

function createTableCampaign(campaignData) {
    var table = $('#campaign');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "responsive": true,
        "language": {
            "decimal": ",",
            "thousands": ".",
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"

        },
        "data": campaignData,

        columns: [
            {data: "name"},
            {data: "created"},
            {data: "start"},
            {data: "end"},
            {data: "type"}
        ],

        "order": [
            [1, 'asc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 30,
        "paging": true,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        }
    })


    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

}
