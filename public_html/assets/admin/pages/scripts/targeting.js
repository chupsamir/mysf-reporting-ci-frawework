/**
 * Created by amir on 20/03/2016.
 */
function targeting() {

    $('#targetingrange').daterangepicker({
            opens: 'left',
            startDate: moment().subtract('days', 29),
            endDate: moment(),
            minDate: '02/01/2016',
            maxDate: '01/01/2017',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'MM/DD/YYYY',
            separator: ' to '
        },

        function (start, end) {
            $('#targetingrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            startDate   =   start.format('YYYY-MM-DD');
            endDate     =   end.format('YYYY-MM-DD');

            Metronic.startPageLoading({animate: true});
            $.ajax({
                url: domain+"targeting/chart_data",
                type: 'POST',
                data: {sd:startDate, ed:endDate},
                dataType: 'json',
                method: 'post',
                success: function(result) {

                    makeChartUrlTraffic(result.url_traffic);
                    createTableTraffic(result.url_traffic);

                    makeChartAppTraffic(result.app_traffic);
                    createTableAppTraffic(result.app_traffic);
                    createChartDataTransfered(result.app_data_transfer);
                    createTableTransfer(result.app_datatables_transfer);
                    createTableAvg(result.app_avg);
                    createTableDuration(result.table_duration);
                    createChartDuration(result.chart_duration);

                    createChartVisited(result.chart_visited);
                    createTableVisited(result.table_visited);
                },
                complete: function() {
                    Metronic.stopPageLoading();
                }
            })
        }
    );

    //important! : Set the initial state of the picker label and ajax request
    fromDate    =   moment().subtract('days', 29);
    toDate      =   moment();

    $('#targetingrange span').html(fromDate.format('MMMM D, YYYY') + ' - ' +toDate.format('MMMM D, YYYY'));

    from   = fromDate.format('YYYY-MM-DD');
    until     = toDate.format('YYYY-MM-DD');

    Metronic.startPageLoading({animate: true});
    $.ajax({
        url: domain+"targeting/chart_data",
        type: 'POST',
        data: {sd:from, ed:until},
        dataType: 'json',
        method: 'post',
        success: function(result) {
            makeChartUrlTraffic(result.url_traffic);
            createTableTraffic(result.url_traffic);

            makeChartAppTraffic(result.app_traffic);
            createTableAppTraffic(result.app_traffic);
            createChartDataTransfered(result.app_data_transfer);
            createTableTransfer(result.app_datatables_transfer);
            createChartappDataAvg(result.chart_avg);
            createTableAvg(result.app_avg);
            createTableDuration(result.table_duration);
            createChartDuration(result.chart_duration);

            createChartVisited(result.chart_visited);
            createTableVisited(result.table_visited);

        },
        complete: function() {
            Metronic.stopPageLoading();
        }
    })
}

function makeChartUrlTraffic(dataTraffic) {
    var chart = AmCharts.makeChart("chart_url_traffic", {
        "type": "pie",
        "theme": "light",
        "fontFamily": 'Open Sans',
        "color":    '#888',
        labelsEnabled: true,
        autoMargins: true,
        marginTop: 4,
        marginBottom: 4,
        marginLeft: 4,
        marginRight: 4,
        pullOutRadius: 20,

        "dataProvider": dataTraffic,

        "valueField": "data",
        "titleField": "interval",
        "outlineAlpha": 0.4,
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
        "angle": 30,
        "exportConfig": {
            menuItems: [{
                icon: '/lib/3/images/export.png',
                format: 'png'
            }]
        }
    });

    jQuery('.chart_7_chart_input').off().on('input change', function() {
        var property = jQuery(this).data('property');
        var target = chart;
        var value = Number(this.value);
        chart.startDuration = 0;

        if (property == 'innerRadius') {
            value += "%";
        }

        target[property] = value;
        chart.validateNow();
    });

    $('#chart_7').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });
}

function createTableTraffic(dataTraffics) {
    var table = $('#data_traffics');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({

        "responsive": true,
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "data": dataTraffics,

        columns: [
            {data: "interval"},
            {data: "data"}
        ],

        "order": [
            [1, 'asc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],

        "pageLength": 10,
        "paging": false,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        }
    })

    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
}

function makeChartAppTraffic(dataApp) {
    var chart = AmCharts.makeChart("chart_app_traffic", {
        "type": "pie",
        "theme": "light",
        "fontFamily": 'Open Sans',
        "color":    '#888',

        labelsEnabled: true,
        autoMargins: true,
        marginTop: 4,
        marginBottom: 4,
        marginLeft: 4,
        marginRight: 4,
        pullOutRadius: 20,

        "dataProvider": dataApp,

        "valueField": "data",
        "titleField": "interval",
        "outlineAlpha": 0.4,
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
        "angle": 30,
        "exportConfig": {
            menuItems: [{
                icon: '/lib/3/images/export.png',
                format: 'png'
            }]
        }
    });

    jQuery('.chart_7_chart_input').off().on('input change', function() {
        var property = jQuery(this).data('property');
        var target = chart;
        var value = Number(this.value);
        chart.startDuration = 0;

        if (property == 'innerRadius') {
            value += "%";
        }

        target[property] = value;
        chart.validateNow();
    });

    $('#chart_7').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });
}

function createTableAppTraffic(dataApp) {
    var table = $('#data_app');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({

        "responsive": true,
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "data": dataApp,

        columns: [
            {data: "interval"},
            {data: "data"}
        ],

        "order": [
            [1, 'asc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],

        "pageLength": 10,
        "paging": false,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        }
    })

    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
}

function createChartDataTransfered (dataTransferred) {

        var chart = AmCharts.makeChart("chart_data_transfered", {
            "type": "serial",
            "theme": "light",


            "handDrawn": true,
            "handDrawScatter": 3,
            "legend": {
                "useGraphSettings": true,
                "markerSize": 8,
                "valueWidth": 0,
                "verticalGap": 0
            },
            "dataProvider": dataTransferred,
            "valueAxes": [{
                "minorGridAlpha": 0.08,
                "minorGridEnabled": true,
                "position": "top",
                "axisAlpha": 0
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b></span>",
                "title": "Data Received",
                "type": "column",
                "fillAlphas": 0.8,

                "valueField": "received"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b></span>",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "fillAlphas": 0,
                "lineThickness": 2,
                "lineAlpha": 1,
                "bulletSize": 7,
                "title": " Data Transferred",
                "valueField": "transferred"
            }],
            "rotate": true,
            "categoryField": "application",
            "categoryAxis": {
                "gridPosition": "start"
            }
        });

        $('#chart_data_transfered').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });

}

function createTableTransfer(appData) {
    var table = $('#app_data');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "responsive": true,
        "language": {
            "decimal": ",",
            "thousands": ".",
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"

        },
        "data": appData,

        columns: [
            {data: "application"},
            {data: "transferred"},
            {data: "received"}
        ],

        "order": [
            [1, 'asc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 30,
        "paging": true,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        }
    })


    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

}

function createChartappDataAvg(appAvg) {
    var chart = AmCharts.makeChart("app_data_avg", {
        "theme": "light",
        "type": "serial",
        "dataProvider": appAvg,
        "valueAxes": [{
            "unit": "Kb",
            "position": "left",
            "title": "Data Transferred/Received",
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "Data Received on [[category]]: <b>[[value]]</b> Kb",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "received",
            "type": "column",
            "valueField": "received"
        }, {
            "balloonText": "Data Transferred on [[category]]: <b>[[value]]</b> Kb",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "Transferred",
            "type": "column",
            "clustered":false,
            "columnWidth":0.5,
            "valueField": "transferred"
        }],
        "plotAreaFillAlphas": 0.1,
        "categoryField": "application",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 30
        },
        "export": {
            "enabled": true
        }

    });
}

function createTableAvg(avgData) {
    var table = $('#app_avg');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "responsive": true,
        "language": {
            "decimal": ",",
            "thousands": ".",
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"

        },
        "data": avgData,

        columns: [
            {data: "application"},
            {data: "transferred"},
            {data: "received"}
        ],

        "order": [
            [1, 'asc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 30,
        "paging": true,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        }
    })


    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

}

function createChartDuration(durationData) {
    var chart = AmCharts.makeChart("chart_duration", {
        "theme": "light",
        "type": "serial",
        "dataProvider": durationData,
        "valueAxes": [{
            "unit": "Min",
            "position": "left",
            "title": "Durations",
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "Duration on [[category]]: <b>[[value]]</b> Minutes",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "Duration",
            "type": "column",
            "valueField": "duration"
        }],
        "plotAreaFillAlphas": 0.1,
        "categoryField": "application",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 30
        },
        "export": {
            "enabled": true
        }

    });
}

function createTableDuration(durationData) {
    var table = $('#duration');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "responsive": true,
        "language": {
            "decimal": ",",
            "thousands": ".",
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"

        },
        "data": durationData,

        columns: [
            {data: "application"},
            {data: "duration"}
        ],

        "order": [
            [1, 'asc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 30,
        "paging": true,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        }
    })


    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

}

function createChartVisited(dataVisited) {
    var chart = AmCharts.makeChart("chart_visited", {
        "theme": "light",
        "type": "serial",
        "startDuration": 2,
        "dataProvider": dataVisited,
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 1,
            "lineAlpha": 0.1,
            "type": "column",
            "valueField": "visited"
        }],
        "depth3D": 20,
        "angle": 30,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "url",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 25
        },
        "export": {
            "enabled": true
        }

    });
    jQuery('.chart-input').off().on('input change',function() {
        var property	= jQuery(this).data('property');
        var target		= chart;
        chart.startDuration = 1;

        if ( property == 'topRadius') {
            target = chart.graphs[0];
            if ( this.value == 0 ) {
                this.value = undefined;
            }
        }

        target[property] = this.value;
        chart.validateNow();
    });
}

function createTableVisited(dataVisited) {
    var table = $('#duration');

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var oTable = table.dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "responsive": true,
        "language": {
            "decimal": ",",
            "thousands": ".",
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"

        },
        "data": dataVisited,

        columns: [
            {data: "url"},
            {data: "visited"}
        ],

        "order": [
            [1, 'desc']
        ],

        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,
        "paging": true,
        "destroy": true,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        }
    })


    var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

}